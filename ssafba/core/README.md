## Source code

This directory contains the *C++* source code for *SSA-FBA*. The directory [emblp](.emblp/) containing classes for embedded LP problems was adapted from the package [dfba](https://gitlab.com/davidtourigny/dynamic-fba) written by the author. The implementation of the Gillespie algorithm in [ssafba](./ssafba.cpp) is partly based on the package [arrow](https://github.com/CovertLab/arrow) from the [Covert lab](https://github.com/CovertLab), in particular their use of the Mersenne Twister pseudo-random number generator code in directory [mersenne](./mersenne/) written by [Christian Stigen Larsen](https://github.com/cslarsen).

