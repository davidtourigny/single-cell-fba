// Copyright (C) 2019, 2020 Columbia University Irving Medical Center,
//     New York, USA

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "emblp.h"
#include <algorithm>

/*
 *------------------------------------------
 * Member functions for embedded LP problem.
 *------------------------------------------
 */

/*
 * Initialization of embedded LP problem
 */

EMBLP::EMBLP(glp_prob *lp_in, glp_smcp parm_in)
{
    ///Here we assign pointer to copy of input LP problem
    lp = glp_create_prob(); //create empty problem and assign pointer
    parm = parm_in; //copy value of input parm
    glp_copy_prob(lp,lp_in,GLP_OFF); //copy problem from input LP problem
    
    ///Here we assign data
    nrow = glp_get_num_rows(lp), ncol = glp_get_num_cols(lp), ntot = ncol+nrow; //set nrow, ncol, ntot
}

/*
 * Set upper bound of lpvariable with provided column index and value
 */

int EMBLP::set_bounds(std::vector<int> col_inds, std::vector<double> ub_values)
{
    if(col_inds.size() != ub_values.size()){
        std::cout << "List of column indices and bound values must be the same size!" << std::endl;
        return -1;
    }
    
    //Here we loop over supplied indices and update bounds
    for(int index=0; index<col_inds.size(); index++){
        int col_index = col_inds[index]; //get column index
        double value = ub_values[index]; //get new bound value
        int type = glp_get_col_type(lp,col_index); //get col type
        if(type == 4 && value == 0.0){ //if ub = lb then col type is changed to GLP_FX
            type = 5;
        }
        if(type == 5 && value != 0.0){ //if ub != lb then col type is changed to GLP_DB
            type = 4;
        }
        glp_set_col_bnds(lp,col_index,type,0.0,value); //update structural lpvariable with new bounds
    }
    int solver = optimize(); //re-optimize
    if(solver != 0){ //if solver unsuccessful
        return solver; //exit simulation
    }
    return 0;
}

/*
 * Perform LP iteration and print results
 */

int EMBLP::optimize()
{
    ///Here we solve embedded LP problem and check primal solution exists
    int solver = glp_simplex(lp,&parm); //optimization using simplex method
    if(solver != 0){ //if simplex method irregular
        std::cout << "Solver flag " << solver << std::endl;
        return solver;
    }
    int primal = glp_get_prim_stat(lp); //get primal status
    if(primal != 2){ //if other than feasible
        std::cout << "Basis not feasible with primal status " << primal << std::endl;
        return 1;
    }
    return 0;
}

/*
 * Return model data
 */

int EMBLP::get_nrow(){ return nrow; }
int EMBLP::get_ncol(){ return ncol; }
int EMBLP::get_ntot(){ return ntot; }

/*
 * Return value of column directly from LP problem
 */

double EMBLP::get_val(int col_index)
{
    return glp_get_col_prim(lp,col_index);
}

/*
 * Return objective value directly from LP problem
 */

double EMBLP::get_obj_val()
{
    return glp_get_obj_val(lp);
}

/*
 * Clear model memory
 */

void EMBLP::clear()
{
    glp_delete_prob(lp);
}

/*
 *-----------------------------------------------
 * Member functions for fast embedded LP problem.
 *-----------------------------------------------
 */

/*
 * Initialization of embedded LP problem
 */

EMBLP_FAST::EMBLP_FAST(glp_prob *lp_in, glp_smcp parm_in) : EMBLP(lp_in, parm_in)
{
    //Here we assign data for fast derived class
    bounds = std::vector<double>(ntot+1); //init bounds to length ntot + 1
    for(int lpindex=1; lpindex<=ntot; lpindex++){ //loop over all lpvariables
        if(lpindex>nrow){ //if structural
            int col_index = lpindex-nrow; //get column index
            bounds[lpindex] = glp_get_col_ub(lp,col_index); //set current ub
        }
        else{ //if auxilliary
            bounds[lpindex] = glp_get_row_ub(lp,lpindex); //set current ub
        }
    }
    basic_vals = std::vector<double>(nrow+1); //init basic values to length nrow+1
    if(optimize() != 0){ //fast algorithm requires initial optimization
        std::cout << "Simplex method failed for initial LP!" << std::endl;
        exit(-1); //exit program
    }
}

/*
 * Set upper bounds of lpvariables with provided column indices and values
 */

int EMBLP_FAST::set_bounds(std::vector<int> col_inds, std::vector<double> ub_values)
{
    if(col_inds.size() != ub_values.size()){
        std::cout << "List of column indices and bound values must be the same size!" << std::endl;
        return -1;
    }
    
    //Here we loop over supplied indices and separate into three categories
    std::vector<int> dep_list; //vector to store lpindices of dependent basic variables
    for(int index=0; index<col_inds.size(); index++){ //loop over provided indices
        int col_index = col_inds[index]; //get column index
        int lpindex = col_index+nrow; //get lpindex
        int status = glp_get_col_stat(lp,col_index); //get status of column
        if(status == 1){ //if column is basic
            dep_list.push_back(lpindex); //add lpindex to dependence list
            std::sort(dep_list.begin(), dep_list.end()); //sort dependence list
            bounds[lpindex] = ub_values[index]; //set new upper bound
        }
        else if(status == 3 || status == 5){ //if column is non-basic fixed on upper bound
            update_basic_vals(lpindex, ub_values[index], dep_list); //calculate new basic variables
            bounds[lpindex] = ub_values[index]; //set new upper bound
        }
        else{ //if column neither basic nor fixed on upper bound
            bounds[lpindex] = ub_values[index]; //set new upper bound
        }
    }
    
    //Here check if new basic values remain within bounds
    if(!check_basic_bounds(dep_list)){ //if basic variable outside bounds
        int solver = optimize(); //re-optimize
            if(solver != 0){ //if solver unsuccessful
                return solver; //exit simulation
            }
            else{ //if solver successful
                return 0;
            }
    }
    return 0;
}

/*
 * Perform LP iteration and print results
 */

int EMBLP_FAST::optimize()
{
    //Here we update bounds before optimizing
    update_bounds();
    
    //Here we solve embedded LP problem and check primal solution exists
    int solver = glp_simplex(lp,&parm); //optimization using simplex method
    if(solver != 0){ //if simplex method irregular
        std::cout << "Solver flag " << solver << std::endl;
        return solver;
    }
    int primal = glp_get_prim_stat(lp); //get primal status
    if(primal != 2){ //if other than feasible
        std::cout << "Basis not feasible with primal status " << primal << std::endl;
        return 1;
    }
    
    //Here we store basic values if solver successful
    store_basic_vals(); //store new basic values and continue
    return 0;
}

/*
 * Update bounds prior to new round of optimization
 */

void EMBLP_FAST::update_bounds()
{
    //Here we update bounds of all structural lpvariables
    for(int i=1; i<=ncol; i++){ //loop over all columns
        int lpindex = i+nrow; //get lpindex of column
        double ub = bounds[lpindex]; //get current ub
        int type = glp_get_col_type(lp,i); //get col type
        if(type == 4 && ub == 0.0){ //if lb = ub then col type is changed to GLP_FX
            type = 5;
        }
        if(type == 5 && ub != 0.0){ //if lb != ub then col type is changed to GLP_DB
            type = 4;
        }
        glp_set_col_bnds(lp,i,type,0.0,ub); //update structural lpvariable with new bounds
    }
}

/*
 * Store basic values
 */

void EMBLP_FAST::store_basic_vals()
{
    for(int k=1; k<=nrow; k++){
        int lpindex = glp_get_bhead(lp,k); //get lpindex of kth basic variable
        if(lpindex>nrow){ //if kth basic variable structural
            basic_vals[k] = glp_get_col_prim(lp,lpindex-nrow); //set kth basic value
        }
        else{ //if kth basic variable auxiliarry
            basic_vals[k] = glp_get_row_prim(lp,lpindex); //set kth basic value
        }
    }
}

/*
 * Check values of basic variables with given indices remain within bounds
 */

bool EMBLP_FAST::check_basic_bounds(std::vector<int> basic_lpindex_list)
{
    bool basis_valid = 1;
    for(int ind=0; ind<basic_lpindex_list.size(); ind++){
        int basic_lpindex = basic_lpindex_list[ind]; //get lpindex of basic variable
        int basic_index = 0;
        if(basic_lpindex > nrow){ //if structural basic lpvariable
            basic_index = glp_get_col_bind(lp,basic_lpindex-nrow); //get basic index of column
        }
        else{ //if auxilliary basic lpvariable
            basic_index = glp_get_row_bind(lp,basic_lpindex); //get basic index of row
        }
        if(basic_vals[basic_index] < 0.0 || basic_vals[basic_index] > bounds[basic_lpindex]){ //if basic value outside bounds
            basis_valid = 0; //basis invalid
            break;
        }
    }
    return basis_valid;
}

/*
 * Update dependent basic values given new upper bound of non-basic lpvariable
 */

void EMBLP_FAST::update_basic_vals(int nbasic_lpindex, double value, std::vector<int> &dep_list)
{
    int ind[nrow+1]; //indices of dependent basic variables
    double tab_val[nrow+1]; //values of corresponding simplex tableau entries
    int len = glp_eval_tab_col(lp,nbasic_lpindex,ind,tab_val); //evaluate column of simplex tableau
    
    //Here we add new dependent basic variables to dependency list
    std::vector<int> updated_list; //this will be created in function
    std::set_symmetric_difference(dep_list.begin(), dep_list.end(), ind + 1, ind + len + 1, std::back_inserter(updated_list));
    dep_list = updated_list; //update dependency list
    
    for(int k=1; k<=len; k++){ //loop over dependent basic variables
        int basic_lpindex = ind[k]; //get lpindex of dependent basic variable
        int basic_index = 0;
        if(basic_lpindex > nrow){ //if structural basic lpvariable
            basic_index = glp_get_col_bind(lp,basic_lpindex-nrow); //get basic index of column
        }
        else{ //if auxilliary basic lpvariable
            basic_index = glp_get_row_bind(lp,basic_lpindex); //get basic index of row
        }
        basic_vals[basic_index] += tab_val[k]*(value - bounds[nbasic_lpindex]); //subract old ub and add new ub
    }
}

/*
 * Return value of column from current basis and bound values
 */

double EMBLP_FAST::get_val(int col_index)
{
    int status = glp_get_col_stat(lp,col_index);
    if(status == 1){ //if column is basic
        int k = glp_get_col_bind(lp,col_index); //get basic index
        return basic_vals[k]; //return current value of basic column
    }
    else if(status == 2){ //if column is non-basic fixed on lower bound
        return 0.0; //return zero
    }
    else if(status == 3 || status == 5){ //if column is non-basic fixed on upper bound
        return bounds[col_index+nrow]; //return current upper bound of non-basic column
    }
    else{ //if unconventional (free)
        std::cout << "Unconventional column!" << std::endl;
        return glp_get_col_prim(lp,col_index);
    }
}

/*
 * Return objective value from current basis and bound values
 */
double EMBLP_FAST::get_obj_val()
{
    double obj_val = 0.0;
    for(int k=1; k<=ncol; k++){
        obj_val += glp_get_obj_coef(lp,k)*get_val(k);
    }
    return obj_val;
}
