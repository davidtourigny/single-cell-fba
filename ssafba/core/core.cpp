// Copyright (C) 2019, 2020 Columbia University Irving Medical Center,
//     New York, USA

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <fstream>
#include <time.h>
#include "core.h"

/*
 * Algorithm 1 for an SSA-FBA simulation.
 
Arguments are:
 * random_state: a seeded MTState pointer for sampling within the algorithm
 
 * emblp: reference to the EMBLP object containing embedded LP problem in SSA-FBA
 
 * substrates: a vector of initial counts for all substrates in model
 
 * stoichiometry_inds: an adjacency list (length #ssa-only + #ssa-fba) containing indices of substrates participating in each reaction
 
 * stoichiometry_inds: an adjacency list (length #ssa-only + #ssa-fba) containing stochiometry of substrates participating in each reaction
 
 * reactants_inds: an adjacency list (length #ssa-only + #fba-bounds) containing indices of reactants required to calculate ssa propensities or fba bounds
 
 * dependencies: an adjacency list (length #ssa-only + #ssa-fba) containing indices of ssa propensities or fba bounds that will be effected by each reaction
 
 * fba_propensities: (length #ssa-fba reactions) containing LP column index corresponding to each LP value to return as FBA propensity
 
 * mu: relative scaling factor for SSA-FBA propensities
 
 * max_events: the maximum number of events to simulate
 
 * duration: the maximum length of time interval to simulate
 
 * delta: the number of steps to skip for approximate method (exact: delta = 0)
 
 * get_ssa_propensity: pointer to function calculating SSA propensities
 
 * set_fba_bound: pointer to function calculating FBA bound
 
 * results: results array to contain substrate counts after each step
 
 * user_data: generic data supplied by user (set to NULL if not used)
 
 
 Returns:
 
 * result_summary object
 
 */

result_summary algorithm1(MTState *random_state,
                          EMBLP &emblp,
                          std::vector<int64_t> &substrates,
                          std::vector< std::vector<int> > stoichiometry_vals,
                          std::vector< std::vector<int> > stoichiometry_inds,
                          std::vector< std::vector<int> > reactants_inds,
                          std::vector< std::vector<int> > dependencies,
                          std::vector<int> fba_propensities,
                          double mu,
                          unsigned int max_events,
                          double duration,
                          unsigned int delta,
                          SsaPropensity get_ssa_propensity,
                          FbaBound get_fba_bounds,
                          std::vector< std::vector<double> > &results,
                          void *user_data
                          )
{
    // These are data to be used within the algorithm
    int reactions_count = stoichiometry_vals.size();
    int fba_rxn_count = fba_propensities.size();
    int ssa_rxn_count = reactions_count - fba_rxn_count;
    int dep_rxn_count = reactants_inds.size();
    std::vector<double> times, propensities(reactions_count);
    std::vector<int> events, update(dep_rxn_count);
    
    // For approximate method we form list of all bounds
    int tot_fba_bounds = dep_rxn_count - ssa_rxn_count;
    std::vector<int> all_fba_bounds(tot_fba_bounds);
    
    // The `update` array will hold for each step which propensities need to be updated
    // for the next time step, based on the dependencies between reactions (sharing
    // substrates). Also set all fba bound indices here.
    for(int reaction=0; reaction<dep_rxn_count; reaction++){
        update[reaction] = reaction;
        if(reaction >= ssa_rxn_count){
            all_fba_bounds[reaction - ssa_rxn_count] = reaction;
        }
    }
    
    // Calculate steps until we reach the provided duration
    unsigned int step = 0;
    double now = 0.0;
    unsigned int delta_step = 0;
    
    while(now < duration){
        
        // First update all propensities that were affected by the previous event
        // we collect the indices of fba bounds to update and do them in one go
        std::vector<int> fba_bounds(0);
        std::vector< std::vector<int64_t> > fba_bound_reactants(0);
        
        for(int up=0; up<update.size(); up++){
            
            // Find which ssa reaction or fba bound to update and obtain reactants
            int reaction = update[up];
            std::vector<int64_t> reactants;
            for(int i=0; i<reactants_inds[reaction].size(); i++){
                int reactant = reactants_inds[reaction][i];
                reactants.push_back(substrates[reactant]);
            }
            
            // If reaction is an ssa reaction, update ssa propensity, otherwise update fba bound
            if(reaction<ssa_rxn_count){
                
                // Go through each reactant for this reaction and calculate its contribution to the propensity
                // based on the counts of the corresponding substrate (set to zero if would generate negative)
                // (TODO: this should be made optional)
                bool zero_propensity = 0;
                for(int i=0; i<stoichiometry_inds[reaction].size(); i++){
                    int substrate_index = stoichiometry_inds[reaction][i];
                    int adjustment = stoichiometry_vals[reaction][i];
                    if(adjustment < 0 && substrates[substrate_index] < -adjustment){
                        zero_propensity = 1;
                        break;
                    }
                }

                if(zero_propensity){
                    propensities[reaction] = 0.0;
                }else{
                    propensities[reaction] = get_ssa_propensity(reaction,reactants,user_data);
                }
            }
            else{
                
                //If the raction is a dependent FBA bound, store it and its reactants
                fba_bounds.push_back(reaction);
                fba_bound_reactants.push_back(reactants);
            }
        }
        if(fba_bounds.size() > 0 && delta == 0){
        
            // If we need to update FBA bounds in exact method
            std::vector<int> indices; //init indices
            std::vector<double> values; //init values
            get_fba_bounds(fba_bounds,fba_bound_reactants,user_data,indices,values); //calculate indices and values
            emblp.set_bounds(indices,values); //set new bounds
            
            // Here need to obtain all new fba propensities
            for(int i=0; i<fba_rxn_count; i++){
                propensities[ssa_rxn_count+i] = mu*emblp.get_val(fba_propensities[i]);
            }
        }else if(delta > 0 && (delta_step == delta || delta_step == 0)){
        
            // If we need to update FBA bounds in approximate method
            std::vector< std::vector<int64_t> > all_reactants(tot_fba_bounds); //init all bound reactants
            for(int bound = 0; bound < tot_fba_bounds; bound++){ //loop over all fba bounds and obtain reactants
                std::vector<int64_t> reactants;
                for(int i=0; i<reactants_inds[ssa_rxn_count+bound].size(); i++){
                    int reactant = reactants_inds[ssa_rxn_count+bound][i];
                    reactants.push_back(substrates[reactant]);
                }
                all_reactants[bound] = reactants; //set reactants for current bound
            }
            std::vector<int> indices; //init indices
            std::vector<double> values; //init values
            get_fba_bounds(all_fba_bounds,all_reactants,user_data,indices,values); //calculate indices and values
            emblp.set_bounds(indices,values); //set new bounds
            delta_step = 0; //reset delta step counter
            
            // Here need to obtain all new fba propensities
            for(int i=0; i<fba_rxn_count; i++){
                propensities[ssa_rxn_count+i] = mu*emblp.get_val(fba_propensities[i]);
            }
        }
        
        // Go through each reactant for all SSA-FBA reactions and calculate its contribution to the propensity
        // based on the counts of the corresponding substrate (set to zero if would generate negative)
        // (TODO: this should be made optional)
        for(unsigned int i=0; i< fba_rxn_count; i++){
            unsigned int reaction = i + ssa_rxn_count;
            bool zero_propensity = 0;
            for(int i=0; i<stoichiometry_inds[reaction].size(); i++){
                int substrate_index = stoichiometry_inds[reaction][i];
                int adjustment = stoichiometry_vals[reaction][i];
                if(adjustment < 0 && substrates[substrate_index] < -adjustment){
                    zero_propensity = 1;
                    break;
                }
            }
            if(zero_propensity){
                propensities[reaction] = 0.0;
            }
        }
        
        // Initial values for next step
        double total = 0.0, interval = 0.0;
        int choice = -1;
        
        // Find the total for all propensities
        for(int reaction=0; reaction<reactions_count; reaction++){
            total += propensities[reaction];
        }
        
        // If the total is zero, then we have no more reactions to perform and can exit early
        if(total <= 0.0){
            break;
        }
        // Otherwise we need to find the next reaction to perform
        else{
            // First, sample two random values, `point` from a linear distribution and
            // `interval` from an exponential distribution.
            interval = sample_exponential(random_state,total);
            double point = sample_uniform(random_state)*total;
            
            // Based on the random sample, find the event that it corresponds to by
            // iterating through the propensities until we surpass our sampled value
            choice = 0;
            double progress = 0.0;
            
            while(progress + propensities[choice] < point){
                progress += propensities[choice];
                choice += 1;
            }
            
            // If we have surpassed the provided duration we can exit now
            if(choice == -1 || (now+interval) > duration){
                break;
            }
            
            // Increase time by the interval sampled above
            now += interval;
            
            // Record the information about the chosen event this step
            times.push_back(now);
            events.push_back(choice);
            
            // For each substrate involved in this reaction, update the ongoing state
            // with its value from the stoichiometric matrix
            for(int i=0; i<stoichiometry_inds[choice].size(); i++){
                int substrate_index = stoichiometry_inds[choice][i];
                int adjustment = stoichiometry_vals[choice][i];
                substrates[substrate_index] += adjustment;
            }
            
            // Find which ssa propensities and fba bounds depend on this reaction and therefore need to be
            // updated in the next step (the rest of the ssa propensities and fba bounds will not change)
            
            update = dependencies[choice];
            
            // Here we store outcome of this step
            std::vector<double> current_substrates(substrates.begin(), substrates.end());
            current_substrates.push_back(emblp.get_obj_val());
            current_substrates.push_back(now);
            results.push_back(current_substrates);
            
            // If the new objective value is zero we exit early (TODO: this should be made optional)
            if(emblp.get_obj_val() == 0.0){
                break;
            }
            
            // Advance
            step += 1;
            delta_step += 1;
            
            // If our step has advanced beyond the current `max_events` we exit now
            if(step >= max_events){
                break;
            }
        }
    }
    
    // Construct the `result_summary` from the results of performing steps until either the desired duration (or maximum number of events) was achieved or there are no more reactions to perform.
    result_summary result = {step, times, events};
    
    // Return the result constructed above
    return result;
}


/*
 * Simulate SSA-FBA model using algorithm 1.
 
 Arguments are:
 
 * results: results array
 
 * lp: pointer to GLPK problem used to initialize EMBLP(_FAST) objects
 
 * parm: glp_smcp object used to used to initialize EMBLP(_FAST) objects
 
 * ssafba_obj: SsaFba object defining the SSA-FBA model to simulate
 
 * substrates: reference to the vector containing counts of all substrates in model
 
 * mu: relative scaling factor for SSA-FBA propensities
 
 * max_events: maximum number of SSA events to simulate before terminating simulation
 
 * duration: maximum length of time to simulate before terminating simulation
 
 * delta: the number of steps to skip for approximate method (exact: delta = 0)
 
 * fast: boolean indicating whether fast algorithm should be used
 
 * user_data: generic data supplied by user (set to NULL if not used)
 
 Returns:
 
 * zero if simulation succesful, non-zero int otherwise
 
 */

int simulate(
             std::vector< std::vector<double> > &results,
             glp_prob *lp,
             glp_smcp parm,
             SsaFba ssafba_obj,
             std::vector<int64_t>& substrates,
             double mu,
             unsigned int max_events,
             double duration,
             unsigned int delta,
             bool fast,
             unsigned int random_seed,
             void *user_data             
             )
{
    // Here we create the MTstate to pass to algorithm 1
    if (random_seed == NULL) {
        random_seed = time(NULL); //use time to seed MTState (not sure this is correct)
    }
    MTState *random_state = (MTState*)malloc(sizeof (MTState)); //allocate memory for MTState
    if(random_state == NULL){ //if failed to allocate the requested block of memory to MTState
        std::cout << "Failed to allocate the requested block of memory to MTState." << std::endl;
        return 1; //return failure of test
    }
    seed(random_state, random_seed); //seed MTState (only needs to be done once before sequence)
    
    // Here we initialize EMBLP(_FAST) object using lp and parm
    EMBLP emblp(lp,parm);
    if(fast){
        emblp = EMBLP_FAST(lp,parm);
    }

    // Here we run the simulation using model specifed by ssafba_obj
    clock_t t1, t2; //time tracking
    t1 = clock(); //set start time
    result_summary output = algorithm1(
                                       random_state,
                                       emblp,
                                       substrates,
                                       ssafba_obj.stoichiometry_vals,
                                       ssafba_obj.stoichiometry_inds,
                                       ssafba_obj.reactants_inds,
                                       ssafba_obj.dependencies,
                                       ssafba_obj.fba_propensities,
                                       mu,
                                       max_events,
                                       duration,
                                       delta,
                                       ssafba_obj.ssa_propensities,
                                       ssafba_obj.fba_bounds,
                                       results,
                                       user_data
                                       );
    t2 = clock(); //set end time
    
    // Here we free memory of MTState
    free(random_state);
    
    // Here we print a summary of algorithm 1
    std::cout << "Run time: " << ((float)t2-(float)t1)/CLOCKS_PER_SEC << std::endl;
    std::cout << "Number of steps: " << output.steps << std::endl;
    std::ofstream events_file;
    events_file.open ("events.txt");
    for(int i=0; i<output.events.size(); i++){
        events_file << output.events[i] << std::endl;
    }
    events_file.close();
    return 0;
}

/*
 * Setting data SSA-FBA simulation.
 
 Arguments are:
 
 * rxn_index: index of SSA-only or SSA-FBA reaction in SsaFba object
 
 * metabolites: list of indices of metabolites participating in given SSA-only or SSA-FBA reaction
 
 * stoichiometries: list of stochiometry values of metabolites participating in given SSA-only or SSA-FBA reaction
 
 * col_index_list: list of GLPK column indices corresponding to SSA-FBA reactions in SsaFba object
 
 * dependents: list of indices of SSA-only reactions or FBA bounds
 
 * rxn_or_bnd_index: index of SSA-only reaction or FBA bound in SsaFba object
 
 * metabolites: list of indices of metabolites that are reactants in a given SSA-only reaction or FBA bound
 
 */

void SsaFba::set_stoichiometry(unsigned int rxn_index, std::vector<int> metabolites, std::vector<int> stoichiometries)
{
    if(rxn_index < stoichiometry_inds.size() && metabolites.size() == stoichiometries.size()){ //if reaction in model and metabolites and stoichiometries compatible
        stoichiometry_inds[rxn_index] = metabolites; //set reaction metabolites
        stoichiometry_vals[rxn_index] = stoichiometries; //set reaction stoichiometries
    }
    else if (metabolites.size() != stoichiometries.size()){
        std::cout << "Length of metabolites must equal length of stoichiometries!" << std::endl;
    }
    else{
        std::cout << "Reaction index greater than total number of SSA-only + SSA-FBA reactions in SsaFba object!" << std::endl;
    }
}

void SsaFba::set_ssafba_indices(std::vector<int> col_index_list)
{
    if(fba_propensities.size() == col_index_list.size()){ //if supplied column index list compatible
        fba_propensities = col_index_list; //set column indices of SSA-FBA reactions
    }
    else{
        std::cout << "List of GLPK column indices must equal number of SSA-FBA reactons in SsaFba object!" << std::endl;
    }
}

void SsaFba::set_dependencies(unsigned int rxn_index, std::vector<int> dependents)
{
    if(dependents.size() <= reactants_inds.size()){ //if dependents list not longer than total number of SSA-only reactions and FBA bounds in model
        dependencies[rxn_index] = dependents;
    }
    else{
        std::cout << "List of dependents must be less that the total number of SSA-only reactons and FBA bounds in SsaFba object!" << std::endl;
    }
}

void SsaFba::set_reactants(unsigned int rxn_or_bnd_index, std::vector<int> reactants)
{
    reactants_inds[rxn_or_bnd_index] = reactants;
}

/*
 * Test speed of EBMLP class vs EMBLP_FAST.
 
 This function selects a random sequence of `NEVNTS' events, each event specifying a number of fluxes (between 1 and `MAX_NPEVNT') whose upper bounds are known (i.e., not zero or infinity) and bounds are to be updates. It then executes two simulations in succession, looping over the sequence and reducing the value of each specified flux's upper bound, and multiplying its previous value by `omega'. The new upper bound values are used to construct an optimal flux distribution. Simulation 1 performs this procedure using the naive approach of re-solving the linear programming (LP) problem after each set of upper bounds are updated. Simulation 2 implements Algorithm 2 described for SSA-FBA.
 
 Arguments are:
 
 * lp: pointer to GLPK problem used to initialize EMBLP(_FAST) objects
 
 * parm: glp_smcp object used to used to initialize EMBLP(_FAST) objects
 
 * omega: the multiplicative factor for multiplying bounds
 
 * NEVNTS: the number of bound update events to simulate
 
 * MAX_NPEVNT: the maximum number of bounds to update per event
 
 */

void test_emblp(glp_prob *lp, glp_smcp parm, double omega, int NEVNTS, int MAX_NPEVNT)
{
    std::vector<int> bound_inds;
    std::vector<double> bound_vals;
    std::vector< std::vector<int> > events(NEVNTS);
    
    //set bound indices and initial values
    for(int i=1; i<=glp_get_num_cols(lp); i++){ //loop over all columns
        double val = glp_get_col_ub(lp,i); //get ub value
        if(val > 0.0 && val < 1000.0){ //if value "known"
            bound_inds.push_back(i); //add column index to bound index
            bound_vals.push_back(val); //add bound value
        }
    }
    int NBOUNDS = bound_inds.size(); //set number of bounds
    
    //generate random order of events with random number of bounds to update (between 1 and MAX_NPEVNT)
    srand (time(NULL));
    for(int t=0; t<NEVNTS; t++){
        std::vector<int> event_seq;
        int num_inds = rand() % MAX_NPEVNT + 1; //generate random integer in range 1 to MAX_NPEVNT
        for(int j=0; j<num_inds; j++){
            int index = rand() % NBOUNDS; //generate random integer in range 0 to NBOUNDS
            event_seq.push_back(index);
        }
        events[t] = event_seq;
    }
    
    //Simulation 1
    EMBLP emblp1(lp,parm); //init EMBLP
    emblp1.optimize(); //test optimization (to obtain current vals)
    clock_t t1, t2; //time tracking
    t1 = clock(); //set start time
    
    for(int t=0; t<NEVNTS; t++){
        
        std::vector<int> event = events[t]; //get event at time t
        std::vector<int> event_inds;
        std::vector<double> event_vals;
        for(int k=0; k<event.size(); k++){
            event_inds.push_back(bound_inds[event[k]]);
            event_vals.push_back(bound_vals[event[k]]);
            bound_vals[event[k]] *= omega;
        }
        int solver = emblp1.set_bounds(event_inds,event_vals); //set bounds
        if(solver != 0){
            std::cout << "Simulation 1 reached non-feasible LP" << std::endl;
            break;
        }
    }
    
    t2 = clock(); //set end time
    std::cout << "Simulation 1 run time: " << ((float)t2-(float)t1)/CLOCKS_PER_SEC << std::endl;
    
    //re-set bound initial values for simulation 2
    for(int i=0; i<NBOUNDS; i++){ //loop over all bounds
        bound_vals[i] = glp_get_col_ub(lp,bound_inds[i]); //get ub value
    }
    
    //Simulation 2
    EMBLP_FAST emblp2(lp,parm); //init EMBLP
    emblp2.optimize(); //test optimization (to obtain current vals)
    clock_t t3, t4; //time tracking
    t3 = clock(); //set start time
    
    for(int t=0; t<NEVNTS; t++){
        
        std::vector<int> event = events[t]; //get event at time t
        std::vector<int> event_inds;
        std::vector<double> event_vals;
        for(int k=0; k<event.size(); k++){
            event_inds.push_back(bound_inds[event[k]]);
            event_vals.push_back(bound_vals[event[k]]);
            bound_vals[event[k]] *= omega;
        }
        int solver = emblp2.set_bounds(event_inds,event_vals); //set bounds
        if(solver != 0){
            std::cout << "Simulation 2 reached non-feasible LP" << std::endl;
            break;
        }
    }
    
    t4 = clock(); //set end time
    std::cout << "Simulation 2 run time: " << ((float)t4-(float)t3)/CLOCKS_PER_SEC << std::endl;
}

/*
 * Toy SSA-FBA simulation.
 This function tests algorithm 1 using the simple reaction scheme s_0 <-> 2 * s_1 -> s_2. In this reaction scheme, reactions v0 and v1 (<->) are simulated using pure SSA, whereas reaction v2 is modelled using FBA. Reaction v2 represents a lumped reaction network: counts of the substrate s_1 for the upper bound of an FBA-only or SSA-FBA reaction, and the propensity value for v2 is given by the SSA-FBA reaction corresponding to `prop_idx', usually growth rate. The function pointers for calculating SSA propensity values for reactions v0, v1 and setting the upper FBA bound for reaction v2 are assigned to functions `simple_reaction' and `simple_bound', respectively, found preceding to the function definition.
 
 Arguments are:
 
 * lp: pointer to GLPK problem used in simulation
 
 * parm: glp_smcp object used to used in simulation
 
 * prop_idx: the index of the SSA-FBA reaction whose propensity value gives that of v_2
 
 * bound_idx: the index of the SSA-FBA or FBA-only reaction used to bound the FBA sub-model
 
 */

double simple_reaction(int reaction, std::vector<int64_t> reactants, void* user_data)
{
    double value = 0.0;
    if(reaction == 0){
        value = reactants[0]*0.0002; //reaction v0 is first order
    }
    else if(reaction == 1){
        value = reactants[0]*reactants[0]*0.0002; //reaction v1 is second order
    }
    return value;
}

void simple_bound(std::vector<int> reaction_array, std::vector< std::vector<int64_t> > reactants_array, void* user_data, std::vector<int> &index, std::vector<double> &value)
{
    int reaction = reaction_array[0];
    std::vector<int64_t> reactants = reactants_array[0];
    if(reaction == 2){
        int bound_idx = *((int*) user_data);
        index = std::vector<int>(1,bound_idx);
        value = std::vector<double>(1,0.1*reactants[0]);
    }
}

void test_simulation(glp_prob *lp, glp_smcp parm, int prop_idx, int bound_idx)
{
    // Here  we initialize numerical arguments for algorithm 1 using reaction s_0 <-> 2 * s_1 -> s_2
    int ssa_only_count = 2, ssa_fba_count = 1, fba_bound_count = 1, max_events = 3000;
    int reactions_count = ssa_only_count + ssa_fba_count;
    double duration = 1.0e6;
    
    // Here we create initial substrate count vector
    std::vector<int64_t> substrates(3);
    substrates[0] = 1000, substrates[1] = 0, substrates[2] = 0;
    
    // Here we initialize the SsaFba object with number of substrates and reactions count
    SsaFba ssafba_obj("toy_model",ssa_only_count,ssa_fba_count,fba_bound_count);
    
    // Here we set stochiometry values for substrates in reactions v0,v1,v2
    ssafba_obj.stoichiometry_vals[0].push_back(-1);
    ssafba_obj.stoichiometry_vals[0].push_back(2);
    ssafba_obj.stoichiometry_vals[1].push_back(1);
    ssafba_obj.stoichiometry_vals[1].push_back(-2);
    ssafba_obj.stoichiometry_vals[2].push_back(-2);
    ssafba_obj.stoichiometry_vals[2].push_back(1);
    
    // Here we set indices of substrates in reactions v0,v1,v2
    ssafba_obj.stoichiometry_inds[0].push_back(0);
    ssafba_obj.stoichiometry_inds[0].push_back(1);
    ssafba_obj.stoichiometry_inds[1].push_back(0);
    ssafba_obj.stoichiometry_inds[1].push_back(1);
    ssafba_obj.stoichiometry_inds[2].push_back(1);
    ssafba_obj.stoichiometry_inds[2].push_back(2);
    
    // Here we set indices of reactants used to calculate ssa propensities/fba bounds
    ssafba_obj.reactants_inds[0].push_back(0);
    ssafba_obj.reactants_inds[1].push_back(1);
    ssafba_obj.reactants_inds[2].push_back(1);
    
    // Here we set indices of ssa propensities/fba bounds that depend on output of v0,v1,v2
    ssafba_obj.dependencies[0].push_back(0);
    ssafba_obj.dependencies[0].push_back(1);
    ssafba_obj.dependencies[0].push_back(2);
    ssafba_obj.dependencies[1].push_back(0);
    ssafba_obj.dependencies[1].push_back(1);
    ssafba_obj.dependencies[1].push_back(2);
    ssafba_obj.dependencies[2].push_back(1);
    ssafba_obj.dependencies[2].push_back(2);

    // Here we set the column indices that correspond to SSA-FBA propensities
    ssafba_obj.fba_propensities[0] = prop_idx; //propensity v_2 (is usually) growth rate
    
    // Here we set function pointers using simple example functions defined below
    ssafba_obj.ssa_propensities = &simple_reaction;
    ssafba_obj.fba_bounds = &simple_bound;
    
    // Here we do the simulation
    std::vector< std::vector<double> > results;
    std::vector<double> init_substrates(substrates.begin(), substrates.end());
    init_substrates.push_back(0.0);
    results.push_back(init_substrates);
    int sim = simulate(results,lp,parm,ssafba_obj,substrates,1.0,max_events,duration,0,1,NULL,&bound_idx);
    if(sim != 0){
        std::cout << "Toy simulation failed!" << std::endl;
    }
}


