// Copyright (C) 2019, 2020 Columbia University Irving Medical Center,
//     New York, USA

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "emblp/emblp.h" //contains glpk.h
#include "mersenne/mersenne.h"

/*
 *-----------------------------------------------
 * Functions and typedefs for simulating SSA-FBA.
 *-----------------------------------------------
 */

/*
 * Typedef for function pointer for calculating SSA propensities
 */

typedef double (*SsaPropensity)(int, std::vector<int64_t>, void*);

/*
 * Typedef for function pointer for setting FBA bounds
 */

typedef void (*FbaBound)(std::vector<int>, std::vector< std::vector<int64_t> >, void*, std::vector<int>&, std::vector<double>&);

/*
 * Struct for holding result of SSA-FBA simulation.
 */

typedef struct {
    unsigned int steps;
    std::vector<double> times;
    std::vector<int> events;
}result_summary;

/*
 * Struct for holding data required for defining SSA-FBA model.
 */

struct SsaFba
{
    SsaFba(
           std::string identifier,                      //unique identifier for model
           unsigned int ssa_only,                       //number of SSA-only reactions in model
           unsigned int ssa_fba,                        //number of SSA-FBA reactions in model
           unsigned int fba_bounds                      //number of dependent FBA bounds in model
           )
    {
        name = identifier;
        stoichiometry_inds = std::vector< std::vector<int> >(ssa_only+ssa_fba);
        stoichiometry_vals = std::vector< std::vector<int> >(ssa_only+ssa_fba);
        reactants_inds = std::vector< std::vector<int> >(ssa_only+fba_bounds);
        dependencies = std::vector< std::vector<int> >(ssa_only+ssa_fba);
        fba_propensities = std::vector<int>(ssa_fba);
    };
    std::string name;                                   //model identifier
    std::vector< std::vector<int> > stoichiometry_inds; //stochiometry indices
    std::vector< std::vector<int> > stoichiometry_vals; //stoichiometry values
    std::vector< std::vector<int> > reactants_inds;     //reactant indices
    std::vector< std::vector<int> > dependencies;       //dependencies
    std::vector<int> fba_propensities;                  //fba propensities
    SsaPropensity ssa_propensities;                     //function pointer for calculating SSA-only propensities
    FbaBound fba_bounds;                                //function pointer for calculating FBA bounds
    void set_stoichiometry(unsigned int, std::vector<int>, std::vector<int>);
    void set_ssafba_indices(std::vector<int>);
    void set_dependencies(unsigned int, std::vector<int>);
    void set_reactants(unsigned int, std::vector<int>);
};

/*
 * Run algorithm 1 on SsaFba object.
 */

int simulate(
             std::vector< std::vector<double> > &,  //results array
             glp_prob *,                            //pointer to GLPK problem
             glp_smcp,                              //parameters for GLPK solver
             SsaFba,                                //SsaFba object to simulate
             std::vector<int64_t>&,                 //reference to substrate counts vector
             double,                                //mu
             unsigned int,                          //max events
             double,                                //duration
             unsigned int,                          //delta
             bool,                                  //fast
             unsigned int,                          //random seed
             void*                                  //user data
             );

/*
 * Invoke Algorithm 1 of a SSA-FBA simulation.
 */

result_summary algorithm1(
                          MTState *,                              //random state
                          EMBLP&,                                 //reference to EMBLP problem
                          std::vector<int64_t>&,                  //reference to substrate counts vector
                          std::vector< std::vector<int> >,        //stoichiometry values
                          std::vector< std::vector<int> >,        //stochiometry indices
                          std::vector< std::vector<int> >,        //reactant indices
                          std::vector< std::vector<int> >,        //dependencies
                          std::vector<int>,                       //fba propensities
                          double,                                 //mu
                          unsigned int,                           //max events
                          double,                                 //duration
                          unsigned int,                           //delta
                          SsaPropensity,                          //function pointer for calculating SSA-only propensities
                          FbaBound,                               //function pointer for calculating FBA bounds
                          std::vector< std::vector <double> > &,  //results array
                          void*                                   //user data
                          );

/*
 *------------------------------
 * Functions for testing module.
 *------------------------------
 */

void test_emblp(glp_prob *, glp_smcp, double, int, int);
void test_simulation(glp_prob *, glp_smcp, int, int);
