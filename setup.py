#!/usr/bin/env python

# Copyright (C) 2019, 2020 David S. Tourigny,
#     Columbia University Irving Medical Center, New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


"""Set up the SSA-FBA package."""

import os
import pathlib
from setuptools import Extension, setup
from setuptools.command.build_ext import build_ext


try:
    DEFAULT_NPROC = len(os.sched_getaffinity(0))
except (OSError, AttributeError):
    DEFAULT_NPROC = 1


class CMakeExtension(Extension):
    def __init__(self, name, sources=(), **kwargs):
        super().__init__(name=name, sources=list(sources), **kwargs)


class CMakeBuild(build_ext):
    def run(self):
        for ext in self.extensions:
            self.build_extension(ext)
        super().run()

    def build_extension(self, extension):
        print("*" * 79)
        cwd = pathlib.Path().absolute()

        # These directories will be created in `build_py`, so if you don't have
        # any Python sources to bundle, the directories will be missing.
        build_temp = pathlib.Path(self.build_temp)
        build_temp.mkdir(parents=True, exist_ok=True)
        ext_dir = pathlib.Path(self.get_ext_fullpath(extension.name))

        # example of cmake args
        config = "Debug" if self.debug else "Release"
        cmake_args = [
            f"-DCMAKE_LIBRARY_OUTPUT_DIRECTORY={ext_dir.parent.absolute()}",
            f"-DCMAKE_BUILD_TYPE={config}",
        ]

        # example of build args
        num_proc = int(os.getenv("NPROC", DEFAULT_NPROC))
        build_args = ["--config", config, "--", "-j", str(num_proc)]

        os.chdir(str(build_temp))
        self.spawn(["cmake", str(cwd)] + cmake_args)
        if not self.dry_run:
            self.spawn(["cmake", "--build", "."] + build_args)
        os.chdir(str(cwd))
        print("*" * 79)


# All other arguments are defined in `setup.cfg`.
setup(
    name='ssafba',
    version='3.7',
    description='Stochastic Simulation Algorithm with Flux-Balance Analysis embedded',
    url='https://gitlab.com/davidtourigny/single-cell-fba',
    download_url='https://gitlab.com/davidtourigny/single-cell-fba/-/releases',
    author='David Tourigny',
    author_email='dst2156@cumc.columbia.edu',
    license='GPL-3.0',
    keywords=', '.join([
        'single-cell',
        'metabolism',
        'gene expression',
        'systems biology',
        'computational biology',
        'discrete kinetic modeling',
        'constraint-based modeling',
        'stochastic simulation algorithm',
        'flux-balance analysis',
        'SSA',
        'FBA',
    ]),    
    packages=['ssafba', 'ssafba.jit'],
    package_dir={'ssafba': 'ssafba'},
    ext_package='ssafba',
    ext_modules=[CMakeExtension("core")],
    cmdclass={"build_ext": CMakeBuild},
    install_requires=[
        'cobra',
    ],
    extras_require={
        'mpneumoniae': [
            'matplotlib',
            'numpy',
            'openpyxl',
            'scipy',
        ]
    },
    tests_require=[],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX',
        'Programming Language :: C++',
        'Programming Language :: Python :: 3 :: Only'
        'Programming Language :: Python :: 3.6'
        'Programming Language :: Python :: 3.7',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'Topic :: Scientific/Engineering :: Mathematics',
    ],
)
