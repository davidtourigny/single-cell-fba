from scipy.constants import Avogadro
import Bio.SeqIO
import csv
import math
import pandas
import wc_utils.util.chem

SEQ_FILENAME = "model.fna"
MODEL_FILENAME = "Model.xlsx"
SPECIES_FILENAME = "model.species.csv"
TRANSLATION_TABLE = 4
ATP = wc_utils.util.chem.EmpiricalFormula('C10H12N5O13P3')
CTP = wc_utils.util.chem.EmpiricalFormula('C9H12N3O14P3')
GTP = wc_utils.util.chem.EmpiricalFormula('C10H12N5O14P3')
UTP = wc_utils.util.chem.EmpiricalFormula('C9H11N2O15P3')
ALA = wc_utils.util.chem.EmpiricalFormula('C3H7NO2')
ARG = wc_utils.util.chem.EmpiricalFormula('C6H15N4O2')
ASP = wc_utils.util.chem.EmpiricalFormula('C4H6NO4')
ASN = wc_utils.util.chem.EmpiricalFormula('C4H8N2O3')
CYS = wc_utils.util.chem.EmpiricalFormula('C3H7NO2S')
GLN = wc_utils.util.chem.EmpiricalFormula('C5H10N2O3')
GLU = wc_utils.util.chem.EmpiricalFormula('C5H8NO4')
GLY = wc_utils.util.chem.EmpiricalFormula('C2H5NO2')
HIS = wc_utils.util.chem.EmpiricalFormula('C6H9N3O2')
ILE = wc_utils.util.chem.EmpiricalFormula('C6H13NO2')
LEU = wc_utils.util.chem.EmpiricalFormula('C6H13NO2')
LYS = wc_utils.util.chem.EmpiricalFormula('C6H15N2O2')
MET = wc_utils.util.chem.EmpiricalFormula('C5H11NO2S')
PHE = wc_utils.util.chem.EmpiricalFormula('C9H11NO2')
PRO = wc_utils.util.chem.EmpiricalFormula('C5H9NO2')
SER = wc_utils.util.chem.EmpiricalFormula('C3H7NO3')
THR = wc_utils.util.chem.EmpiricalFormula('C4H9NO3')
TRP = wc_utils.util.chem.EmpiricalFormula('C11H12N2O2')
TYR = wc_utils.util.chem.EmpiricalFormula('C9H11NO3')
VAL = wc_utils.util.chem.EmpiricalFormula('C5H11NO2')
PPi = wc_utils.util.chem.EmpiricalFormula('O7P2')
H2O = wc_utils.util.chem.EmpiricalFormula('H2O')
H = wc_utils.util.chem.EmpiricalFormula('H')
VOL = 4.58E-17  # l
CELL_CYCLE_LEN = 28800  # s

genome_seq = next(Bio.SeqIO.parse(SEQ_FILENAME, "fasta"))

species = pandas.read_excel(MODEL_FILENAME, sheet_name="Species", header=1)

species_props = {}
biomass = {
    'ATP': 0,
    'CTP': 0,
    'GTP': 0,
    'UTP': 0,
    'GDP': 0,
    'AMP': 0,
    'CMP': 0,
    'GMP': 0,
    'UMP': 0,
    'ALA': 0,
    'ARG': 0,
    'ASP': 0,
    'ASN': 0,
    'CYS': 0,
    'GLN': 0,
    'GLU': 0,
    'GLY': 0,
    'HIS': 0,
    'ILE': 0,
    'LEU': 0,
    'LYS': 0,
    'MET': 0,
    'PHE': 0,
    'PRO': 0,
    'SER': 0,
    'THR': 0,
    'TRP': 0,
    'TYR': 0,
    'VAL': 0,
    'PPi': 0,
    'Pi': 0,
    'H2O': 0,
    'H': 0,
}
results = []

for _, specie in species.iterrows():
    if specie['Type'] in ['Metabolite']:
        if specie['Id'] in biomass and not math.isnan(specie['Cytosol (M)']):
            biomass[specie['Id']] += 0 * specie['Cytosol (M)'] * Avogadro * VOL

    elif specie['Type'] in ['RNA', 'Protein']:
        start = int(specie['Coordinate (nt)'] - 1)
        end = start + int(specie['Length (nt)']) - 1
        gene_seq = genome_seq[start:end + 1].seq
        if specie['Direction'] == 'r':
            gene_seq = gene_seq.reverse_complement()

        if specie['Type'] == 'RNA':
            species_seq = gene_seq.transcribe()

            a = species_seq.count('A')
            c = species_seq.count('C')
            g = species_seq.count('G')
            u = species_seq.count('U')
            n_nmps = a + c + g + u

            formula = ATP * a + CTP * c + GTP * g + UTP * u - PPi * n_nmps - H * (n_nmps - 1)
            charge = a * -4 + c * -4 + g * -4 + u * -4 - n_nmps * -4 - (n_nmps - 1) * 1

            specie_props = {
                'formula': formula,
                'molecular_weight': formula.get_molecular_weight(),
                'charge': charge,
                'degradation_lhs': {
                    'H2O': n_nmps - 1,
                },
                'degradation_rhs': {
                    'AMP': a,
                    'CMP': c,
                    'GMP': g,
                    'UMP': u,
                    'H': n_nmps - 1,
                },
            }
            species_props[specie['Id']] = specie_props
            
            degradation = (specie['Free copies per cell'] * math.log(2)) * math.log(2)  / (specie['Free half-life (min)'] * 60)
            if not math.isnan(specie['Copies in complexes per cell']):
                degradation += (specie['Copies in complexes per cell'] * math.log(2)) * math.log(2) / (specie['Half-life in complex (min)'] * 60)
            synthesis = degradation + (specie['Total copies per cell'] * math.log(2)) * math.log(2) / CELL_CYCLE_LEN

            biomass['ATP'] += a * synthesis
            biomass['CTP'] += c * synthesis
            biomass['GTP'] += g * synthesis
            biomass['UTP'] += u * synthesis
            biomass['PPi'] -= n_nmps * synthesis
            biomass['H2O'] += synthesis
            biomass['H'] -= synthesis

            biomass['AMP'] -= a * degradation
            biomass['CMP'] -= c * degradation
            biomass['GMP'] -= g * degradation
            biomass['UMP'] -= u * degradation
            biomass['H2O'] += (n_nmps - 1) * degradation
            biomass['H'] -= (n_nmps - 1) * degradation

            results.append({
                'Id': specie['Id'],
                'Type': specie['Type'],
                'Structure': str(species_seq),
                'Chemical formula': str(formula),
                'Molecular weight': formula.get_molecular_weight(),
                'Charge': charge,
                'Synthesis': ('[c]: '
                              '({}) ATP + '
                              '({}) CTP + '
                              '({}) GTP + '
                              '({}) UTP + '
                              '({}) H '
                              '<=> {} + ({}) PPi').format(
                    a, c, g, u, 1, specie['Id'], n_nmps),
                'Degradation': ('[c]: {} + {} <=> {}').format(
                    specie['Id'],
                    ' + '.join('({}) {}'.format(coeff, specie_id) for specie_id, coeff in specie_props['degradation_lhs'].items()),
                    ' + '.join('({}) {}'.format(coeff, specie_id) for specie_id, coeff in specie_props['degradation_rhs'].items()),
                ),
            })

        else:
            species_seq = gene_seq.translate(table=TRANSLATION_TABLE, to_stop=True)

            ala = species_seq.count('A')
            arg = species_seq.count('R')
            asp = species_seq.count('D')
            asn = species_seq.count('N')
            cys = species_seq.count('C')
            gln = species_seq.count('Q')
            glu = species_seq.count('E')
            gly = species_seq.count('G')
            his = species_seq.count('H')
            ile = species_seq.count('I')
            leu = species_seq.count('L')
            lys = species_seq.count('K')
            met = species_seq.count('M')
            phe = species_seq.count('F')
            pro = species_seq.count('P')
            ser = species_seq.count('S')
            thr = species_seq.count('T')
            trp = species_seq.count('W')
            tyr = species_seq.count('Y')
            val = species_seq.count('V')
            n_aas = ala + arg + asp + asn + cys + gln + glu + gly + his + ile \
                + leu + lys + met + phe + pro + ser + thr + trp + tyr + val
            assert n_aas == len(species_seq)

            formula = ALA * ala + ARG * arg + ASP * asp + ASN * asn + CYS * cys \
                + GLN * gln + GLU * glu + GLY * gly + HIS * his + ILE * ile\
                + LEU * leu + LYS * lys + MET * met + PHE * phe + PRO * pro\
                + SER * ser + THR * thr + TRP * trp + TYR * tyr + VAL * val \
                - H2O * (n_aas - 1)
            charge = ala * 0 + arg * 1 + asp * -1 + asn * 0 + cys * 0 \
                + gln * 0 + glu * -1 + gly * 0 + his * 0 + ile * 0 \
                + leu * 0 + lys * 1 + met * 0 + phe * 0 + pro * 0 \
                + ser * 0 + thr * 0 + trp * 0 + tyr * 0 + val * 0 \
                - (n_aas - 1) * 0

            specie_props = {
                'formula': formula,
                'molecular_weight': formula.get_molecular_weight(),
                'charge': charge,
                'degradation_lhs': {
                    'H2O': n_aas - 1,
                },
                'degradation_rhs': {
                    'ALA': ala,
                    'ARG': arg,
                    'ASP': asp,
                    'ASN': asn,
                    'CYS': cys,
                    'GLN': gln,
                    'GLU': glu,
                    'GLY': gly,
                    'HIS': his,
                    'ILE': ile,
                    'LEU': leu,
                    'LYS': lys,
                    'MET': met,
                    'PHE': phe,
                    'PRO': pro,
                    'SER': ser,
                    'THR': thr,
                    'TRP': trp,
                    'TYR': tyr,
                    'VAL': val,
                },
            }
            species_props[specie['Id']] = specie_props
            
            degradation = (math.log(2) * specie['Free copies per cell'])* math.log(2) / (specie['Free half-life (min)'] * 60)
            if not math.isnan(specie['Copies in complexes per cell']):
                degradation += (math.log(2) * specie['Copies in complexes per cell']) * math.log(2) / (specie['Half-life in complex (min)'] * 60)
            synthesis = degradation + (specie['Total copies per cell'] * math.log(2)) * math.log(2) / CELL_CYCLE_LEN

            biomass['ALA'] += ala * (synthesis - degradation)
            biomass['ARG'] += arg * (synthesis - degradation)
            biomass['ASP'] += asp * (synthesis - degradation)
            biomass['ASN'] += asn * (synthesis - degradation)
            biomass['CYS'] += cys * (synthesis - degradation)
            biomass['GLN'] += gln * (synthesis - degradation)
            biomass['GLU'] += glu * (synthesis - degradation)
            biomass['GLY'] += gly * (synthesis - degradation)
            biomass['HIS'] += his * (synthesis - degradation)
            biomass['ILE'] += ile * (synthesis - degradation)
            biomass['LEU'] += leu * (synthesis - degradation)
            biomass['LYS'] += lys * (synthesis - degradation)
            biomass['MET'] += met * (synthesis - degradation)
            biomass['PHE'] += phe * (synthesis - degradation)
            biomass['PRO'] += pro * (synthesis - degradation)
            biomass['SER'] += ser * (synthesis - degradation)
            biomass['THR'] += thr * (synthesis - degradation)
            biomass['TRP'] += trp * (synthesis - degradation)
            biomass['TYR'] += tyr * (synthesis - degradation)
            biomass['VAL'] += val * (synthesis - degradation)
            biomass['GTP'] += (2 * n_aas + 3) * synthesis
            biomass['H2O'] += (2 * n_aas + 3) * synthesis - (n_aas - 1) * (synthesis - degradation)
            biomass['GDP'] -= (2 * n_aas + 3) * synthesis
            biomass['Pi'] -= (2 * n_aas + 3) * synthesis
            biomass['H'] -= (2 * n_aas + 3) * synthesis

            results.append({
                'Id': specie['Id'],
                'Type': specie['Type'],
                'Structure': str(species_seq),
                'Chemical formula': str(formula),
                'Molecular weight': formula.get_molecular_weight(),
                'Charge': charge,
                'Synthesis': ('[c]: '
                              '({}) ALA + '
                              '({}) ARG + '
                              '({}) ASP + '
                              '({}) ASN + '
                              '({}) CYS + '
                              '({}) GLN + '
                              '({}) GLU + '
                              '({}) GLY + '
                              '({}) HIS + '
                              '({}) ILE + '
                              '({}) LEU + '
                              '({}) LYS + '
                              '({}) MET + '
                              '({}) PHE + '
                              '({}) PRO + '
                              '({}) SER + '
                              '({}) THR + '
                              '({}) TRP + '
                              '({}) TYR + '
                              '({}) VAL + '
                              '({}) GTP + '
                              '({}) H2O <=> {} + ({}) GDP + ({}) Pi + ({}) H').format(
                    ala, arg, asp, asn, cys, gln, glu, gly, his, ile,
                    leu, lys, met, phe, pro, ser, thr, trp, tyr, val,
                    2 * n_aas + 3,
                    2 * n_aas + 3 - (n_aas - 1),
                    specie['Id'],
                    2 * n_aas + 3,
                    2 * n_aas + 3,
                    2 * n_aas + 3),
                'Degradation': '[c]: {} + {} <=> {}'.format(
                    specie['Id'],
                    ' + '.join('({}) {}'.format(coeff, specie_id) for specie_id, coeff in specie_props['degradation_lhs'].items()),
                    ' + '.join('({}) {}'.format(coeff, specie_id) for specie_id, coeff in specie_props['degradation_rhs'].items()),
                ),
            })

    elif specie['Type'] == 'Complex':
        formula = wc_utils.util.chem.EmpiricalFormula()
        charge = 0

        subunits = specie['Structure'].split(' + ')
        degradation_lhs = {}
        degradation_rhs = {}
        for subunit in subunits:
            subunit_stoich, _, subunit_id = subunit.rpartition(' ')
            if subunit_stoich:
                subunit_stoich = int(subunit_stoich[1:-1])
            else:
                subunit_stoich = 1
            formula += species_props[subunit_id]['formula'] * subunit_stoich
            charge += species_props[subunit_id]['charge'] * subunit_stoich

            for species, coeff in species_props[subunit_id]['degradation_lhs'].items():
                if species not in degradation_lhs:
                    degradation_lhs[species] = 0
                degradation_lhs[species] += coeff * subunit_stoich

            for species, coeff in species_props[subunit_id]['degradation_rhs'].items():
                if species not in degradation_rhs:
                    degradation_rhs[species] = 0
                degradation_rhs[species] += coeff * subunit_stoich

        results.append({
            'Id': specie['Id'],
            'Type': specie['Type'],
            'Structure': specie['Structure'],
            'Chemical formula': str(formula),
            'Molecular weight': formula.get_molecular_weight(),
            'Charge': charge,
            'Synthesis': None,
            'Degradation': ('[c]: {} + {} <=> {}').format(
                specie['Id'],
                ' + '.join('({}) {}'.format(coeff, specie_id) for specie_id, coeff in degradation_lhs.items()),
                ' + '.join('({}) {}'.format(coeff, specie_id) for specie_id, coeff in degradation_rhs.items()),
            ),
        })

# biomass
biomass['Biomass'] = -1
biomass_lhs = []
biomass_rhs = []
for species, coeff in biomass.items():
    if abs(coeff) == 1:
        term = species
    else:
        term = '({}) {}'.format(abs(coeff), species)

    if coeff > 0:
        biomass_lhs.append(term)
    else:
        biomass_rhs.append(term)

biomass_eq = '[c]: {} <=> {}'.format(' + '.join(biomass_lhs), ' + '.join(biomass_rhs))

# save results to file
with open(SPECIES_FILENAME, 'w') as file:
    writer = csv.DictWriter(file, fieldnames=['Id', 'Type',
                                              'Structure', 'Chemical formula', 'Molecular weight', 'Charge',
                                              'Synthesis', 'Degradation'], dialect='excel')
    writer.writeheader()
    for result in results:
        writer.writerow(result)

print(biomass_eq)
