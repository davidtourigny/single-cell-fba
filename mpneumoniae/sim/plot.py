""" Utility functions for plotting simulation results

:Author: Jonathan Karr <karr@mssm.edu>
:Date: 2020-06-11
:Copyright: 2016-2020, Karr Lab
:License: MIT
"""

from matplotlib.backends.backend_pdf import PdfPages
from model import Model, SpeciesType, Submodel
from scipy.constants import Avogadro
from matplotlib import pyplot, ticker
import numpy
import os
import re


def plot_results(model, time, vol, growth, species_counts, output_dir_name):
    """ Plot simulation results

    Args:
        model (:obj:`Model`): model
        time (:obj:`numpy.ndarray`): array of time
        vol (:obj:`numpy.ndarray`): array of cellular volumes
        growth (:obj:`numpy.ndarray`): array of growth rates
        species_counts (:obj:`numpy.ndarray`): matrix of species counts; rows=species, columns=time points
        output_dir_name (:obj:`str`): path to save plots
    """
    if not os.path.exists(output_dir_name):
        os.makedirs(output_dir_name)

    cell_comp = model.get_component_by_id('c', model.compartments)

    tot_rna = numpy.zeros(len(time))
    tot_rna_mass = numpy.zeros(len(time))
    tot_prot_monomers = numpy.zeros(len(time))
    tot_prot_monomer_mass = numpy.zeros(len(time))
    tot_complexes = numpy.zeros(len(time))
    tot_complex_mass = numpy.zeros(len(time))
    for species in model.species:
        species_count = species_counts[species.index, cell_comp.index, :]
        if species.type == SpeciesType.rna:
            tot_rna += species_count
            tot_rna_mass += species_count * species.mol_wt / Avogadro * 1e15
        elif species.type == SpeciesType.protein:
            tot_prot_monomers += species_count
            tot_prot_monomer_mass += species_count * species.mol_wt / Avogadro * 1e15
        elif species.type == SpeciesType.complex:
            tot_complexes += species_count
            tot_complex_mass += species_count * species.mol_wt / Avogadro * 1e15

    plot_timecourse(
        model=model,
        time=time,
        y_datas={'Growth': growth},
        units='cell s^{-1}',
        show_y_zero=True,
        file_name=os.path.join(output_dir_name, 'Growth.pdf')
    )

    plot_timecourse(
        model=model,
        time=time,
        y_datas={'RNA': tot_rna},
        units='molecules',
        show_y_zero=True,
        file_name=os.path.join(output_dir_name, 'Total RNA by count.pdf')
    )

    plot_timecourse(
        model=model,
        time=time,
        y_datas={'RNA': tot_rna_mass},
        units='fg',
        show_y_zero=True,
        file_name=os.path.join(output_dir_name, 'Total RNA by mass.pdf')
    )

    plot_timecourse(
        model=model,
        time=time,
        y_datas={
            'Monomers': tot_prot_monomers,
            'Complexes': tot_complexes,
        },
        units='molecules',
        show_y_zero=True,
        file_name=os.path.join(output_dir_name, 'Total protein by count.pdf')
    )

    plot_timecourse(
        model=model,
        time=time,
        y_datas={
            'Monomers': tot_prot_monomer_mass,
            'Complexes': tot_complex_mass,
            'Total': tot_prot_monomer_mass + tot_complex_mass,
        },
        units='fg',
        show_y_zero=True,
        file_name=os.path.join(output_dir_name, 'Total protein by mass.pdf')
    )

    plot_timecourse(
        model=model,
        time=time,
        vol=vol,
        species_counts=species_counts,
        units='uM',
        species_comp_ids=['ATP[c]', 'CTP[c]', 'GTP[c]', 'UTP[c]'],
        file_name=os.path.join(output_dir_name, 'NTPs.pdf')
    )

    plot_timecourse(
        model=model,
        time=time,
        vol=vol,
        species_counts=species_counts,
        species_comp_ids=['ALA[c]', 'ARG[c]', 'ASN[c]', 'ASP[c]'],
        units='uM',
        file_name=os.path.join(output_dir_name, 'Amino acids.pdf')
    )

    plot_timecourse(
        model=model,
        time=time,
        species_counts=species_counts,
        species_comp_ids=[
            'Apt-Protein[c]',
            'Apt-Complex[c]',
            'PeptAbcTransporter-A1-Protein[c]',
            'PeptAbcTransporter-A2-Protein[c]',
            'PeptAbcTransporter-T1-Protein[c]',
            'PeptAbcTransporter-T2-Protein[c]',
            'PeptAbcTransporter-Complex[c]',
        ],
        units='molecules',
        show_y_zero=True,
        file_name=os.path.join(output_dir_name, 'Complexes and subunits.pdf')
    )

    plot_timecourse(
        model=model,
        time=time,
        species_counts=species_counts,
        species_comp_ids=[
            'Gk-Complex[c]',
            'RnaPolymerase-Complex[c]',
            'Ribosome-Complex[c]',
            'Oligoribonuclease-Protein[c]',
        ],
        units='molecules',
        show_y_zero=True,
        file_name=os.path.join(output_dir_name, 'Key enzymes.pdf')
    )


def plot_timecourse(model, time=numpy.zeros(0), y_datas=None,
                    species_counts=None, vol=numpy.zeros(0), species_comp_ids=None,
                    show_y_zero=False, units='mM', title=None, file_name=None):
    """
    Plot one or more timecourses of species or other variables and

    Args:
        model (:obj:`Model`): model
        time (:obj:`numpy.ndarray`): array of time
        y_datas (:obj:`dict` of :obj:`str` => :obj:`numpy.ndarray`, optional): dictionary that maps labels to timecourses to plot
        species_counts (:obj:`numpy.ndarray`): matrix of species counts; rows=species, columns=time points
        vol (:obj:`numpy.ndarray`): array of cellular volumes
        species_comp_ids (:obj:`list` of :obj:`str`, optional): ids of species/compartments to plot in addition to the data in
            :obj:`y_datas`
        show_y_zero (:obj:`bool`, optional): if :obj:`True`, set the y minimum to be at most zero
        units (:obj:`str`, optional): units
        title (:obj:`str`, optional): figure title
        file_name (:obj:`str`, optional): path to save file
    """
    y_datas = y_datas or {}
    species_comp_ids = species_comp_ids or []

    # convert time to hours
    time = time.copy() / 3600

    # create figure
    fig = pyplot.figure()

    # extract data to plot
    for species_comp_id in species_comp_ids:
        # extract data
        match = re.match(r'^(?P<species_id>[a-z0-9\-_]+)\[(?P<comp_id>[a-z0-9\-_]+)\]$', species_comp_id, re.I).groupdict()
        species_id = match['species_id']
        comp_id = match['comp_id']

        if isinstance(model, Model):
            species = model.get_component_by_id(species_id, model.species)
            compartment = model.get_component_by_id(comp_id, model.compartments)
            y_data = species_counts[species.index, compartment.index, :]
        elif isinstance(model, Submodel):
            y_data = species_counts[species_comp_id]
        else:
            raise ValueError('Invalid model type {}'.format(model.__class__.__name__))

        # scale
        if comp_id != 'c':
            raise ValueError('compartment must have id `c`')

        if units == 'pM':
            scale = 1 / Avogadro / vol * 1e12
        elif units == 'nM':
            scale = 1 / Avogadro / vol * 1e9
        elif units == 'uM':
            scale = 1 / Avogadro / vol * 1e6
        elif units == 'mM':
            scale = 1 / Avogadro / vol * 1e3
        elif units == 'M':
            scale = 1 / Avogadro / vol * 1e0
        elif units == 'molecules':
            scale = 1
        else:
            raise ValueError('Invalid units "{}"'.format(units))

        y_data *= scale

        y_datas[species_comp_id] = y_data

    # plot timecourses
    y_min = 0.0
    y_max = 0.0
    for label, y_data in y_datas.items():
        # update range
        y_min = numpy.nanmin([y_min, numpy.nanmin(y_data)])
        y_max = numpy.nanmax([y_max, numpy.nanmax(y_data)])

        # add to plot
        pyplot.plot(time, y_data, label=label)

    if show_y_zero:
        y_min = numpy.nanmin([y_min, 0])

    # set axis limits
    pyplot.xlim((0, time[-1]))
    pyplot.ylim((y_min, y_max))

    # add axis labels and legend
    if title:
        pyplot.title(title)

    pyplot.xlabel('Time (h)')

    if units == 'molecules':
        pyplot.ylabel('Copy number')
    elif units in ['M', 'mM', 'uM', 'nM', 'pM']:
        pyplot.ylabel('Concentration ({})'.format(units))
    elif units == 'fg':
        pyplot.ylabel('Mass ({})'.format(units))
    elif units == 'al':
        pyplot.ylabel('Volume ({})'.format(units))
    elif len(y_datas) == 1:
        pyplot.ylabel('{} ({})'.format(list(y_datas.keys())[0], units))
    else:
        raise ValueError('Unable to handle y-label')

    y_formatter = ticker.ScalarFormatter(useOffset=False)
    pyplot.gca().get_yaxis().set_major_formatter(y_formatter)

    if len(y_datas) > 1:
        pyplot.legend()

    # save plot to file
    if file_name:
        pp = PdfPages(file_name)
        pp.savefig(fig)
        pp.close()
