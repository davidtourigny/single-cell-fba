# Copyright (C) 2019, 2020 Columbia University Irving Medical Center,
#     New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# required libraries
from model import read_model_from_file, SsaSubmodel, FbaSubmodel, ReactionParticipant
from plot import plot_results
from scipy.constants import Avogadro
import argparse
import csv
import numpy
import os.path
import ssafba
import ssafba.jit
import sys
import time

# default model file
MODEL_FILENAME = os.path.join(os.path.dirname(__file__), 'Model.xlsx')

# ids of key species
AMINO_ACIDS = [
    'ALA[c]',
    'ARG[c]',
    'ASP[c]',
    'ASN[c]',
    'CYS[c]',
    'GLN[c]',
    'GLU[c]',
    'GLY[c]',
    'HIS[c]',
    'ILE[c]',
    'LEU[c]',
    'LYS[c]',
    'MET[c]',
    'PHE[c]',
    'PRO[c]',
    'SER[c]',
    'THR[c]',
    'TRP[c]',
    'TYR[c]',
    'VAL[c]',
]
NTPS = [
    'ATP[c]',
    'CTP[c]',
    'GTP[c]',
    'UTP[c]',
]
GTP = 'GTP[c]'
RNASE = 'Oligoribonuclease-Protein[c]'
RNA_POLYMERASE = 'RnaPolymerase-Complex[c]'
RIBOSOME = 'Ribosome-Complex[c]'
PROTEASE = 'Lon-Complex[c]'

# useful macros
DEFAULT_MAX_EVENTS = int(2e6)
SPECIES_CONCS_START = "species_concs['"
SPECIES_CONCS_END = "']"
FBA_STOCH = 200

def get_glpk_indices(submodel, reaction_id):
    # Get GLPK indices for forward and reverse reaction in an FbaSubmodel

    if not isinstance(submodel, FbaSubmodel):
        raise ValueError("Submodel must be an instance of FbaSubmodel to get GLPK indices")
    reaction = submodel.get_component_by_id(reaction_id, submodel.reactions)
    if reaction:
        f_index = 2 * submodel.cobra_model.reactions.index(reaction_id) + 1
        r_index = f_index + 1
        return (f_index, r_index)
    else:
        raise ValueError(reaction_id + " must belong to FbaSubmodel " + submodel.id)


class ExchangeReaction:
    # Custom class for an exchange reaction
    def __init__(self, ex_species, metabolism_submodel):
        cobra_index = ex_species.reaction_index
        self.id = metabolism_submodel.cobra_model.reactions[cobra_index].id
        species_id = ex_species.id.replace('[e]', '')
        ex_part = ReactionParticipant(species_id, 'e', coefficient=-1 * FBA_STOCH)
        in_part = ReactionParticipant(species_id, 'c', coefficient=1 * FBA_STOCH)
        ex_part.id = ex_species.id
        in_part.id = ex_species.id.replace('[e]', '[c]')
        self.participants = [ex_part, in_part]
        self.glpk_indices = (2 * cobra_index + 1, 2 * cobra_index + 2)


class SsaFbaReaction:
    # Custom class for an SSA-FBA reaction
    def __init__(self, name, species_id, coefficient, glpk_index):
        self.id = name
        part = ReactionParticipant(species_id, 'c', coefficient=coefficient * FBA_STOCH)
        part.id = species_id
        self.participants = [part]
        self.glpk_index = glpk_index


def simulate(model, duration=None, adk_vmax=None, max_events=DEFAULT_MAX_EVENTS, random_seed=None):
    # Simulate model
    
    # get the model volume
    volume = model.get_component_by_id('c', model.compartments).init_vol

    # get the simulation duration
    if duration is None:
        duration = model.get_component_by_id('cellCycleLength', model.parameters).value
        
    # update ADK v_max
    if adk_vmax:
        model.get_component_by_id("AK_GTP", model.reactions).v_max = adk_vmax

    # seed the random number generator
    if random_seed is None:
        random_seed = int(time.time()*1e9) % 2 ** 32
    numpy.random.seed(random_seed)
    print(random_seed)

    """ Sample initial species counts using Poisson distribution. Store indices in dictionary """
    counts_dict = model.get_species_counts_dict()
    species_counts = []
    species_indices = {}
    for i_species, (species_id, mean_count) in enumerate(counts_dict.items()):
        species_indices[species_id] = i_species
        count = numpy.random.poisson(mean_count)
        species_counts.append(count)

    """This is the metabolic submodel"""

    # set metabolic submodel and initialize
    metabolism_submodel = model.get_component_by_id('Metabolism', model.submodels)
    metabolism_submodel.update_local_cell_state()

    """These are the SSA-only reactions"""

    # get reactions from all SsaSubmodels
    ssa_only_reactions = []
    for submodel in model.submodels:
        if isinstance(submodel, SsaSubmodel):
            ssa_only_reactions.extend(submodel.reactions)

    """These are the SSA-FBA reactions"""

    # get reactions producing/consuming amino acids/NTPs from FBA model
    amino_acid_fba_reactions = []
    ntp_fba_reactions = []
    for rxn in metabolism_submodel.reactions:
        if rxn.id != 'MetabolismProduction':
            cobra_index = metabolism_submodel.cobra_model.reactions.index(rxn.id)

            for part in rxn.participants:

                if part.id in AMINO_ACIDS:
                    amino_acid_fba_reactions.append(SsaFbaReaction(rxn.id, part.id, part.coefficient, 2 * cobra_index + 1))
                    if rxn.reversible:
                        amino_acid_fba_reactions.append(SsaFbaReaction(rxn.id + "Rev", part.id, -part.coefficient, 2 * cobra_index + 2))

                if part.id in NTPS:
                    ntp_fba_reactions.append(SsaFbaReaction(rxn.id, part.id, part.coefficient, 2 * cobra_index + 1))
                    if rxn.reversible:
                        ntp_fba_reactions.append(SsaFbaReaction(rxn.id + "Rev", part.id, -part.coefficient, 2 * cobra_index + 2))

    """These are the exchange FBA reactions (both dependent FBA reactions and SSA-FBA reactions)"""

    # get FBA exchange reactions excluding H2O
    exchange_fba_reactions = []
    for ex_species in metabolism_submodel.ex_species:
        if ex_species.id != 'H2O[e]':
            rxn = ExchangeReaction(ex_species, metabolism_submodel)
            exchange_fba_reactions.append(rxn)

    """These are the dependent FBA reactions"""

    # get FBA reactions with enzyme and/or those that consume NTPs or amino acids
    dependent_fba_reactions = []
    for rxn in metabolism_submodel.reactions:
        if rxn.id != 'MetabolismProduction':
            if rxn.rate_law:
                dependent_fba_reactions.append(rxn)
    dependent_fba_reactions = list(set(dependent_fba_reactions))

    """Init SsaFba object"""

    # initialize SsaFba object with name "mpneumoniae"
    ssa_fba_model = ssafba.SsaFba(
        "mpneumoniae",
        len(ssa_only_reactions),
        len(amino_acid_fba_reactions) + len(ntp_fba_reactions) + len(exchange_fba_reactions),
        len(dependent_fba_reactions) + len(exchange_fba_reactions),
    )

    """These are all the SSA-only and the SSA-FBA reactions"""

    # add SSA-FBA reactions to list and store indices in dictionary
    all_reactions = ssa_only_reactions.copy()
    all_reactions.extend(amino_acid_fba_reactions)
    all_reactions.extend(ntp_fba_reactions)
    all_reactions.extend(exchange_fba_reactions)
    reaction_indices = {}
    for i in range(len(all_reactions)):
        reaction_indices[all_reactions[i].id] = i

    """These are all the SSA-only reactions and dependent/exchange FBA reactions"""

    # add dependent FBA reactions to list and store indices in dictionary
    all_dependents = ssa_only_reactions.copy()
    all_dependents.extend(dependent_fba_reactions)
    all_dependents.extend(exchange_fba_reactions)
    dependent_indices = {}
    for i in range(len(all_dependents)):
        dependent_indices[all_dependents[i].id] = i

    """These are the participants and the stochiometry of each reaction"""

    # set participants and the stochiometry of each reaction
    for rxn in all_reactions:
        participants = []
        coefficients = []
        for part in rxn.participants:
            participants.append(species_indices[part.id])
            coeff = int(part.coefficient)
            coefficients.append(coeff)
        ssa_fba_model.set_stoichiometry(reaction_indices[rxn.id], participants, coefficients)

    """These are the generic indices of reactants for reactions in SSA submodels"""

    # get indices for transcription reactants
    transcription_reactant_indices = []
    for nuc in NTPS:
        transcription_reactant_indices.append(species_indices[nuc])
    transcription_reactant_indices.append(species_indices[RNA_POLYMERASE])

    # get indices for translation reactants
    translation_reactant_indices = []
    for aa in AMINO_ACIDS:
        translation_reactant_indices.append(species_indices[aa])
    translation_reactant_indices.append(species_indices[RIBOSOME])

    # get indices for rna degredation reactants
    rnadegredation_reactant_indices = []
    rnadegredation_reactant_indices.append(species_indices[RNASE])

    # get indices for protein degredation reactants
    prtdegredation_reactant_indices = []
    prtdegredation_reactant_indices.append(species_indices[PROTEASE])

    """This is the dictonary that keeps track of reactants"""

    # generate reactants dictionary
    reactants_dict = {}
    for species in model.species:
        for compartment in model.compartments:
            reactants_dict['%s[%s]' % (species.id, compartment.id)] = []

    """These are the reactants of the SSA-only reactions"""

    # set reactants for SSA-only reactions
    for rxn in ssa_only_reactions:
        if rxn.rate_law:
            submodel_id = rxn.submodel.id
            reactant_indices = []
            rxn_index = dependent_indices[rxn.id]

            if submodel_id == 'Complexation':
                for part in rxn.participants:
                    if part.coefficient < 0:
                        species_key = part.id
                        species_index = species_indices[species_key]
                        reactant_indices.append(species_index)
                        reactants_dict[species_key].append(rxn_index)

            if submodel_id == 'ProteinDegradation':
                reactant_indices = prtdegredation_reactant_indices.copy()
                species_key = rxn.rate_law.compiled_expr.split()[2]
                species_key = species_key[species_key.find(SPECIES_CONCS_START) + len(SPECIES_CONCS_START):species_key.rfind(SPECIES_CONCS_END)]
                species_index = species_indices[species_key]
                reactant_indices.append(species_index)
                reactants_dict[species_key].append(rxn_index)

            if submodel_id == 'RnaDegradation':
                reactant_indices = rnadegredation_reactant_indices.copy()
                species_key = rxn.rate_law.compiled_expr.split()[2]
                species_key = species_key[species_key.find(SPECIES_CONCS_START) + len(SPECIES_CONCS_START):species_key.rfind(SPECIES_CONCS_END)]
                species_index = species_indices[species_key]
                reactant_indices.append(species_index)
                reactants_dict[species_key].append(rxn_index)

            if submodel_id == 'Transcription':
                reactant_indices = transcription_reactant_indices.copy()

            if submodel_id == 'Translation':
                reactant_indices = translation_reactant_indices.copy()
                species_key = rxn.rate_law.compiled_expr.split()[122]
                species_key = species_key[species_key.find(SPECIES_CONCS_START) + len(SPECIES_CONCS_START):species_key.rfind(SPECIES_CONCS_END)]
                species_index = species_indices[species_key]
                reactant_indices.append(species_index)
                reactant_indices.sort()
                reactants_dict[species_key].append(rxn_index)

            ssa_fba_model.set_reactants(rxn_index, reactant_indices)

    """These are the reactants of the dependent FBA bounds"""

    # set reactants for dependent FBA bounds using compiled expr rate_law for enzymes
    for rxn in dependent_fba_reactions:
        reactant_indices = []
        rxn_index = dependent_indices[rxn.id]

        if rxn.rate_law:
            species_key = rxn.rate_law.compiled_expr.split()[2]
            species_key = species_key[species_key.find(SPECIES_CONCS_START) + len(SPECIES_CONCS_START):species_key.rfind(SPECIES_CONCS_END)]
            species_index = species_indices[species_key]
            reactant_indices.append(species_index)
            reactant_indices.sort()
            reactants_dict[species_key].append(rxn_index)

        ssa_fba_model.set_reactants(rxn_index, reactant_indices)

    """These are the reactants of the exchange FBA bounds"""

    # set reactants for exchange FBA bounds
    for rxn in exchange_fba_reactions:
        reactant_indices = []
        rxn_index = dependent_indices[rxn.id]

        species_key = rxn.participants[0].id
        species_index = species_indices[species_key]
        reactant_indices.append(species_index)
        reactant_indices.sort()
        reactants_dict[species_key].append(rxn_index)

        ssa_fba_model.set_reactants(rxn_index, reactant_indices)

    """These are general dependents"""

    # collect dependent reactions
    translation_dependents = []
    transcription_dependents = []
    for rxn in ssa_only_reactions:
        if rxn.rate_law:
            submodel_id = rxn.submodel.id
            rxn_index = dependent_indices[rxn.id]

            if submodel_id == 'Translation':
                translation_dependents.append(rxn_index)

            if submodel_id == 'Transcription':
                transcription_dependents.append(rxn_index)

    """These are the dependents of the SSA-only reactions"""

    # set dependents for SSA-only reactions
    for rxn in ssa_only_reactions:
        dependents = []
        rxn_index = reaction_indices[rxn.id]
        submodel_id = rxn.submodel.id

        for part in rxn.participants:
            
            if part.id == PROTEASE:
                submodel = model.get_component_by_id('ProteinDegradation', model.submodels)
                for dep in submodel.reactions:
                    dependents.append(dependent_indices[dep.id])

            if part.id == RNASE:
                submodel = model.get_component_by_id('RnaDegradation', model.submodels)
                for dep in submodel.reactions:
                    dependents.append(dependent_indices[dep.id])

            if part.id == RNA_POLYMERASE:
                submodel = model.get_component_by_id('Transcription', model.submodels)
                for dep in submodel.reactions:
                    dependents.append(dependent_indices[dep.id])

            if part.id == RIBOSOME:
                submodel = model.get_component_by_id('Translation', model.submodels)
                for dep in submodel.reactions:
                    dependents.append(dependent_indices[dep.id])

            dependents.extend(reactants_dict[part.id])

        # add all general dependents since these reactions consume/produce NTPs and/or AAs
        if submodel_id == 'ProteinDegradation':
            dependents.extend(translation_dependents)
        
        if submodel_id == 'Transcription':
            dependents.extend(transcription_dependents)
            dependents.extend(translation_dependents)

        if submodel_id == 'Translation':
            dependents.extend(transcription_dependents)
            dependents.extend(translation_dependents)

        dependents = sorted(list(set(dependents)))
        ssa_fba_model.set_dependencies(rxn_index, dependents)

    """These are the dependents of the SSA-FBA reactions"""

    # set dependents for amino acid reactions (all Translation reactions)
    for rxn in amino_acid_fba_reactions:
        dependents = []
        dependents.extend(translation_dependents)
        dependents = sorted(list(set(dependents)))
        ssa_fba_model.set_dependencies(reaction_indices[rxn.id], dependents)

    # set dependents for nucleotide reactions
    # (all Transcription reactions and Translation iff GTP produced/consumed, and other nucleotide FBA bounds)
    for rxn in ntp_fba_reactions:
        dependents = []
        dependents.extend(transcription_dependents)
        for part in rxn.participants:
            if part.id == GTP:
                dependents.extend(translation_dependents)
            if part.id in NTPS:
                dependents.extend(reactants_dict[part.id])

        dependents = sorted(list(set(dependents)))
        ssa_fba_model.set_dependencies(reaction_indices[rxn.id], dependents)

    # set dependents for exchanges reactions
    for rxn in exchange_fba_reactions:
        dependents = []
        for part in rxn.participants:
            dependents.extend(reactants_dict[part.id])

        dependents = sorted(list(set(dependents)))
        ssa_fba_model.set_dependencies(reaction_indices[rxn.id], dependents)

    """These are the GLPK indices of the SSA-FBA reactions"""

    # set GLPK indices of SSA-FBA reactions and exchange reactions
    glpk_indices = []

    for rxn in amino_acid_fba_reactions:
        glpk_indices.append(rxn.glpk_index)

    for rxn in ntp_fba_reactions:
        glpk_indices.append(rxn.glpk_index)

    for rxn in exchange_fba_reactions:
        glpk_indices.append(rxn.glpk_indices[0])

    ssa_fba_model.set_ssafba_indices(glpk_indices)

    """This is the dynamic library generation"""

    # open library and write header
    cpp_file_path = os.path.join("functionlib.cpp")
    with open(cpp_file_path, "w") as cpp_file:
        write_header(cpp_file, volume)

    # collect data for writing SSA-only propensity functions
    vmax = ["0.0"] * len(ssa_only_reactions)
    km1 = ["0.0"] * len(ssa_only_reactions)
    km2 = ["0.0"] * len(ssa_only_reactions)
    submodel_counts = { 'Complexation': 0, 'ProteinDegradation': 0, 'RnaDegradation': 0, 'Transcription': 0 , 'Translation': 0}
    for rxn in ssa_only_reactions:
        submodel_id = rxn.submodel.id
        submodel_counts[submodel_id] += 1
        if rxn.rate_law:
            rxn_index = reaction_indices[rxn.id]
            vmax[rxn_index] = str(rxn.v_max)
            if submodel_id != 'Complexation':
                km1[rxn_index] = str(rxn.k_m_1)
            if submodel_id == 'Translation':
                km2[rxn_index] = str(rxn.k_m_2)

    # open library and write SSA-only propensity functions
    with open(cpp_file_path, "a") as cpp_file:
        write_ssaonly_function(cpp_file, submodel_counts, vmax, km1, km2)

    # collect data for writing dependent FBA reaction bound functions
    kineticsvmax = ["0.0"] * len(dependent_fba_reactions)
    glpk_indices = ["0"] * 2 * (len(dependent_fba_reactions) + len(exchange_fba_reactions))
    len_ssa_only = str(len(ssa_only_reactions))
    len_fba_bounds = str(len(dependent_fba_reactions))
    for rxn in dependent_fba_reactions:
        rxn_index = dependent_indices[rxn.id] - len(ssa_only_reactions)
        if rxn.v_max:
            kineticsvmax[rxn_index] = str(rxn.v_max)
        else:
            kineticsvmax[rxn_index] = str(metabolism_submodel.cobra_model.reactions.get_by_id(rxn.id).upper_bound)

        f_index, r_index = get_glpk_indices(metabolism_submodel, rxn.id)
        glpk_indices[2 * rxn_index] = str(f_index)
        glpk_indices[2 * rxn_index + 1] = str(r_index)

    for rxn in exchange_fba_reactions:
        rxn_index = dependent_indices[rxn.id] - len(ssa_only_reactions)
        f_index, r_index = rxn.glpk_indices
        glpk_indices[2 * rxn_index] = str(f_index)
        glpk_indices[2 * rxn_index + 1] = str(r_index)

    # open library and write dependent FBA bound functions
    with open(cpp_file_path, "a") as cpp_file:
        write_bounds_function(cpp_file, kineticsvmax, glpk_indices, len_ssa_only, len_fba_bounds)

    # compile dynamic library
    ssafba.jit.compile()

    """This is the simulation of the model"""
    results = ssafba.simulate_model(metabolism_submodel.cobra_model, ssa_fba_model, species_counts,
                                    max_events, duration, mu=1/FBA_STOCH, random_seed=random_seed)

    """This is the final organization of results"""
    n_time_steps = len(results)
    time_hist = numpy.zeros(n_time_steps)
    vol_hist = numpy.ones(n_time_steps)*volume
    growth_hist = numpy.ones(n_time_steps)
    species_count_hist = numpy.zeros((len(model.species), len(model.compartments), n_time_steps))
    for i_time in range(n_time_steps):
        growth_hist[i_time] = metabolism_submodel.growth_rate_scale * results[i_time][len(species_counts)]
        time_hist[i_time] = results[i_time][len(species_counts) + 1]
        mass = 0.
        for species in model.species:
            for compartment in model.compartments:
                species_index = species_indices['{}[{}]'.format(species.id, compartment.id)]
                species_count_hist[species.index, compartment.index, i_time] = results[i_time][species_index]
    growth_hist[0] = numpy.nan

    # downsample simulation predictions to 1 s frequency
    down_time_step = 10.
    down_time_max = numpy.floor(time_hist[-1] / down_time_step) * down_time_step
    n_down_time_steps = int(numpy.floor(time_hist[-1] / down_time_step) + 1)
    down_time_hist = numpy.linspace(0., down_time_max, n_down_time_steps)
    down_vol_hist = numpy.full((n_down_time_steps), numpy.nan)
    down_growth_hist = numpy.full((n_down_time_steps), numpy.nan)
    down_species_count_hist = numpy.full((len(model.species), len(model.compartments), n_down_time_steps), numpy.nan)
    for i_down_time, sim_time in enumerate(down_time_hist):
        i_time = numpy.where(time_hist <= sim_time)[0][-1]
        down_vol_hist[i_down_time] = vol_hist[i_time]
        down_growth_hist[i_down_time] = growth_hist[i_time]
        down_species_count_hist[:, :, i_down_time] = species_count_hist[:, :, i_time]

    # save results to CSV file
    output_dir_name = os.path.join(os.path.dirname(__file__), 'results')
    if not os.path.exists(output_dir_name):
        os.makedirs(output_dir_name)

    with open(os.path.join(output_dir_name, 'results.csv'), 'w') as file:
        writer = csv.writer(file)
        writer.writerow(['Variable', 'Compartment', 'Type', 'Subtype'] + [None] * len(down_time_hist))
        writer.writerow(['time', None, None, None] + list(down_time_hist))
        writer.writerow(['growth', None, None, None] + list(down_growth_hist))
        writer.writerow(['volume', None, None, None] + list(down_vol_hist))
        for species, specie_comps_count_hist in zip(model.species, down_species_count_hist):
            for comp, specie_comp_count_hist in zip(model.compartments, specie_comps_count_hist):
                if numpy.any(specie_comp_count_hist):
                    writer.writerow([species.id, comp.id, 'Species', species.type.name] + list(specie_comp_count_hist))

    plot_results(model, down_time_hist, down_vol_hist, down_growth_hist, down_species_count_hist, output_dir_name)


def write_indent(cpp_file, indent_count, text):
    while indent_count > 0:
        text = "    " + text
        indent_count -= 1
    cpp_file.write(text)


def write_header(cpp_file, volume):
    cpp_file.write("#include <iostream>\n#include <algorithm>\n#include <vector>\n#define VOLUME_c " + str(volume) + "\n#define N_AVOGADRO " +
                   str(Avogadro) + "\n\ndouble min(std::vector<double> object)\n")
    write_indent(cpp_file, 0, "{\n")
    write_indent(cpp_file, 1, "int n = object.size();\n")
    write_indent(cpp_file, 1, "double min = 0.0;\n")
    write_indent(cpp_file, 1, "if(object[0] > 0.0){\n")
    write_indent(cpp_file, 2, "min = object[0];\n")
    write_indent(cpp_file, 1, "}else{\n")
    write_indent(cpp_file, 2, "return min;\n")
    write_indent(cpp_file, 1, "}\n")

    write_indent(cpp_file, 1, "for(int i=1; i<n; i++){\n")
    write_indent(cpp_file, 2, "if(object[i] < min && object[i] >= 0.0){\n")
    write_indent(cpp_file, 3, "min = object[i];\n")
    write_indent(cpp_file, 2, "}\n")
    write_indent(cpp_file, 1, "}\n")
    write_indent(cpp_file, 1, "return min;\n")
    write_indent(cpp_file, 0, "}\n\n")


def write_ssaonly_function(
    cpp_file,
    submodel_counts,
    kineticsvmax,
    kineticskm1,
    kineticskm2,
    model_name="mpneumoniae"
):

    write_indent(cpp_file, 0, "std::vector<double> vmax = {")
    for i in range(len(kineticsvmax)):
        write_indent(cpp_file, 0, kineticsvmax[i] + ", ")
    write_indent(cpp_file, 0, "};\n\n")
    write_indent(cpp_file, 0, "std::vector<double> km1 = {")
    for i in range(len(kineticskm1)):
        write_indent(cpp_file, 0, kineticskm1[i] + ", ")
    write_indent(cpp_file, 0, "};\n\n")
    write_indent(cpp_file, 0, "std::vector<double> km2 = {")
    for i in range(len(kineticskm2)):
        write_indent(cpp_file, 0, kineticskm2[i] + ", ")
    write_indent(cpp_file, 0, "};\n\n")
    write_indent(
        cpp_file,
        0,
        'extern "C" double ssa_propensities_'
        + model_name
        + "(int reaction, std::vector<int64_t> reactants, void* user_data)\n",
    )
    write_indent(cpp_file, 0, "{\n")
    write_indent(cpp_file, 1, "double value = 0.0;\n")
    write_indent(cpp_file, 1, "std::vector<double> concentrations(reactants.size());\n\n")
    write_indent(cpp_file, 1, "for(unsigned int i=0; i<reactants.size(); i++){\n")
    write_indent(cpp_file, 2, "concentrations[i] = reactants[i]/(VOLUME_c*N_AVOGADRO);\n")
    write_indent(cpp_file, 1, "}\n\n")

    write_indent(cpp_file, 1, "if(reaction < " + str(submodel_counts['Complexation']) + "){\n")
    write_indent(cpp_file, 2, "value = vmax[reaction]*min(concentrations);\n")
    write_indent(cpp_file, 1, "}\n\n")
    write_indent(cpp_file, 1, "else if(reaction < " + str(submodel_counts['Complexation'] + submodel_counts['ProteinDegradation']) + "){\n")
    write_indent(cpp_file, 2, "value = vmax[reaction]*concentrations[0]*concentrations[1]/(km1[reaction] + concentrations[1]);\n")
    write_indent(cpp_file, 1, "}\n\n")
    write_indent(cpp_file, 1, "else if(reaction < " + str(submodel_counts['Complexation'] + submodel_counts['ProteinDegradation'] + submodel_counts['RnaDegradation']) + "){\n")
    write_indent(cpp_file, 2, "value = vmax[reaction]*concentrations[0]*concentrations[1]/(km1[reaction] + concentrations[1]);\n")
    write_indent(cpp_file, 1, "}\n\n")
    write_indent(cpp_file, 1, "else if(reaction < " + str(submodel_counts['Complexation'] + submodel_counts['ProteinDegradation'] + submodel_counts['RnaDegradation'] + submodel_counts['Transcription']) + "){\n")
    write_indent(cpp_file, 2, "double substrates = vmax[reaction];\n")
    write_indent(cpp_file, 2, "for(unsigned int i=0; i<4; i++){\n")
    write_indent(cpp_file, 3, "substrates *= concentrations[i]/(concentrations[i] + km1[reaction]);\n")
    write_indent(cpp_file, 2, "}\n")
    write_indent(cpp_file, 2, "value = substrates*concentrations[4];\n")
    write_indent(cpp_file, 1, "}\n\n")
    write_indent(cpp_file, 1, "else if(reaction < " +
                 str(submodel_counts['Complexation'] + submodel_counts['ProteinDegradation'] + submodel_counts['RnaDegradation'] + submodel_counts['Transcription'] + submodel_counts['Translation']) + "){\n")
    write_indent(cpp_file, 2, "double substrates = vmax[reaction];\n")
    write_indent(cpp_file, 2, "for(unsigned int i=0; i<20; i++){\n")
    write_indent(cpp_file, 3, "substrates *= concentrations[i]/(km1[reaction] + concentrations[i]);\n")
    write_indent(cpp_file, 2, "}\n")
    write_indent(cpp_file, 2, "value = substrates*concentrations[21]*concentrations[20]/(km2[reaction] + concentrations[20]);\n")
    write_indent(cpp_file, 1, "}\n\n")
    write_indent(cpp_file, 1, "if(value < 0.0){\n")
    write_indent(cpp_file, 2, "value = 0.0;\n")
    write_indent(cpp_file, 1, "}\n")
    write_indent(cpp_file, 1, "return (VOLUME_c*N_AVOGADRO)*value;\n")
    write_indent(cpp_file, 0, "}\n\n")


def write_bounds_function(
    cpp_file,
    kineticsvmax,
    glpk_indices,
    len_ssa_only,
    len_fba_bounds,
    model_name="mpneumoniae"
):

    write_indent(cpp_file, 0, "std::vector<double> vmax_enzyme = {")
    for i in range(len(kineticsvmax)):
        write_indent(cpp_file, 0, kineticsvmax[i] + ", ")
    write_indent(cpp_file, 0, "};\n\n")
    write_indent(cpp_file, 0, "std::vector<int> glpk_indices = {")
    for i in range(len(glpk_indices)):
        write_indent(cpp_file, 0, glpk_indices[i] + ", ")
    write_indent(cpp_file, 0, "};\n\n")
    write_indent(
        cpp_file,
        0,
        'extern "C" void fba_bounds_'
        + model_name
        + "(std::vector<int> reaction_array, std::vector< std::vector<int64_t> >  reactants_array, void* user_data, std::vector<int> &index, std::vector<double> &value)\n",
    )
    write_indent(cpp_file, 0, "{\n")
    write_indent(cpp_file, 1, "for(unsigned int i=0; i<reaction_array.size(); i++){\n")
    write_indent(cpp_file, 2, "int bound = reaction_array[i] - " + len_ssa_only + ";\n")
    write_indent(cpp_file, 2, "if(bound <" + len_fba_bounds + "){\n")
    write_indent(cpp_file, 3, "std::vector<double> reactants(reactants_array[i].begin(), reactants_array[i].end());\n")
    write_indent(cpp_file, 3, "double bound_value = vmax_enzyme[bound]*reactants[0];\n")
    write_indent(cpp_file, 3, "index.push_back(glpk_indices[2*bound]);\n")
    write_indent(cpp_file, 3, "value.push_back(bound_value);\n")
    write_indent(cpp_file, 2, "}\n")
    write_indent(cpp_file, 2, "else{\n")
    write_indent(cpp_file, 3, "double metabolite = reactants_array[i][0];\n")
    write_indent(cpp_file, 3, "double bound_value = std::max(metabolite,0.0);\n")
    write_indent(cpp_file, 3, "index.push_back(glpk_indices[2*bound]);\n")
    write_indent(cpp_file, 3, "value.push_back(bound_value);\n")
    write_indent(cpp_file, 2, "}\n")
    write_indent(cpp_file, 1, "}\n")
    write_indent(cpp_file, 0, "}\n\n")

def main():
    # Main function
    parser = argparse.ArgumentParser(
        description='Simulate a SSA-FBA model of the gene expression and metabolism of individual Mycoplasma pneumoniae cells')
    parser.add_argument('--model-filename', type=str, default=MODEL_FILENAME,
                        help='Path to model file (default: {})'.format(os.path.relpath(MODEL_FILENAME, os.curdir)))
    parser.add_argument('--duration', type=int, default=None, help='Duration in seconds (default: doubling time = 8 h)')
    parser.add_argument('--adk-vmax', type=float, default=None, help='Overwrite vmax for ADK GTP reaction (default: use the coefficient from Model input file)')
    parser.add_argument('--max-events', type=int, default=DEFAULT_MAX_EVENTS, help='Maximum number of events to simulate (default: {})'.format(DEFAULT_MAX_EVENTS))
    parser.add_argument('--random-seed', type=int, default=None, help='Random number generator seed (default: use the current time)')
    args = parser.parse_args()

    model = read_model_from_file(args.model_filename)
    simulate(model, duration=args.duration, adk_vmax=args.adk_vmax, max_events=args.max_events, random_seed=args.random_seed)


if __name__ == "__main__":
    main()
