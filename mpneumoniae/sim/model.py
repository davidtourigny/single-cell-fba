""" Reas models specified in an XLSX spreadsheet into a Python data structure

:Author: Jonathan Karr <karr@mssm.edu>
:Date: 2020-06-11
:Copyright: 2016-2020, Karr Lab
:License: MIT
"""

from cobra import Metabolite as CobraMetabolite
from cobra import Model as CobraModel
from cobra import Reaction as CobraReaction
from openpyxl import load_workbook
from scipy.constants import Avogadro
import enum
import math
import numpy
import util
import re
import warnings


class Model(object):
    """ Represents a model

    Attributes:
        submodels (:obj:`list of :obj:`Submodel`): submodels
        compartments (:obj:`list of :obj:`Compartment`): compartments
        species (:obj:`list of :obj:`Species`): species
        reactions (:obj:`list of :obj:`Reaction`): reactions
        parameters (:obj:`list of :obj:`Parameter`): parameters
        references (:obj:`list of :obj:`Reference`): references

        frac_dry_wt (:obj:`float`): fraction of cell mass that is dry (not water)
        _density (:obj:`float`): cellular density

        _species_counts (:obj:`numpy.ndarray`): current species counts
        _mass (:obj:`float`): current cellular mass
        _dry_wt (:obj:`float`): current cellular dry weight
        _vol (:obj:`float`): current cellular volume
        _extracellular_vol (:obj:`float`): current extracellular volume
        _growth (:obj:`float`): current growth rate
    """

    def __init__(self, submodels=None, compartments=None, species=None, reactions=None, parameters=None, references=None):
        """
        Args:
            submodels (:obj:`list of :obj:`Submodel`): submodels
            compartments (:obj:`list of :obj:`Compartment`): compartments
            species (:obj:`list of :obj:`Species`): species
            reactions (:obj:`list of :obj:`Reaction`): reactions
            parameters (:obj:`list of :obj:`Parameter`): parameters
            references (:obj:`list of :obj:`Reference`): references
        """
        self.submodels = submodels or []
        self.compartments = compartments or []
        self.species = species or []
        self.reactions = reactions or []
        self.parameters = parameters or []
        self.references = references or []

        self.density = None
        self.frac_dry_wt = None

        self._species_counts = numpy.zeros(0)  # rows: species, columns: compartments
        self._mass = None
        self._dry_wt = None
        self._vol = None
        self._extracellular_vol = None
        self._growth = None

    def setup(self):
        """ Setup simulation

        * Retrieve parameter values
        * Setup submodels
        * Calculate initial conditions
        """

        self.frac_dry_wt = self.get_component_by_id('fractionDryWeight', self.parameters).value

        for submodel in self.submodels:
            submodel.setup()

        self.calc_init_conditions()

    def calc_init_conditions(self):
        """ Calculate initial conditions """
        cell_comp = self.get_component_by_id('c', self.compartments)
        extr_comp = self.get_component_by_id('e', self.compartments)

        # volume
        self._vol = cell_comp.init_vol
        self._extracellular_vol = extr_comp.init_vol

        # species counts
        self._species_counts = numpy.zeros((len(self.species), len(self.compartments)))
        for species in self.species:
            for conc in species.concentrations:
                self._species_counts[species.index, conc.compartment.index] = conc.value * conc.compartment.init_vol * Avogadro

        # cell mass
        self.calc_mass()

        # density
        self._density = self._mass / self._vol

        # growth
        self._growth = numpy.nan

        # sync submodels
        for submodel in self.submodels:
            submodel.update_local_cell_state()

    def calc_mass(self):
        """ Calculate current cellular mass """
        for comp in self.compartments:
            if comp.id == 'c':
                i_cell_comp = comp.index

        mass = 0.
        for species in self.species:
            if species.mol_wt is not None:
                mass += self._species_counts[species.index, i_cell_comp] * species.mol_wt
        mass /= Avogadro

        self._mass = mass
        self._dry_wt = self.frac_dry_wt * mass

    def set_component_indices(self):
        for index, obj in enumerate(self.submodels):
            obj.index = index
        for index, obj in enumerate(self.compartments):
            obj.index = index
        for index, obj in enumerate(self.species):
            obj.index = index
        for index, obj in enumerate(self.reactions):
            obj.index = index
        for index, obj in enumerate(self.parameters):
            obj.index = index
        for index, obj in enumerate(self.references):
            obj.index = index

    def get_species_counts(self):
        """ Get current species counts

        Returns:
            :obj:`numpy.ndarray`: current species counts
        """
        return self._species_counts

    def get_species_counts_dict(self):
        """ Get species counts as a dictionary

        Returns:
            :obj:`dict`: dictionary that maps species-compartment ids to their counts
        """

        counts_dict = {}
        for species in self.species:
            for compartment in self.compartments:
                counts_dict['{}[{}]'.format(species.id, compartment.id)] = self._species_counts[species.index, compartment.index]
        return counts_dict

    def get_component_by_id(self, id, components):
        """ Get the model component with id :obj:`id`

        Args:
            id (:obj:`str`): component id
            components (:obj:`list` of :obj:`object`): list of components to search over

        Returns:
            :obj:`object`: component
        """
        for component in components:
            if component.id == id:
                return component


class Submodel(object):
    """ Represents a submodel (species, reactions, parameters, references)

    Attributes:
        model (:obj:`Model`): model
        id (:obj:`str`): id
        name (:obj:`str`): name
        index (:obj:`int`): index of the submodel within the list of submodels
        species (:obj:`list of :obj:`Species`): species
        reactions (:obj:`list of :obj:`Reaction`): reactions
        parameters (:obj:`list of :obj:`Parameter`): parameters
        algorithm (:obj:`str`): simulation algorithm
        _species_counts (:obj:`numpy.ndarray`): current species counts
        _vol (:obj:`float`): current cellular volume
        _extracellular_vol (:obj:`float`): current extracellular volume
    """

    def __init__(self, model, id=None, name=None, species=None, reactions=None, algorithm=None):
        """
        Args:
            model (:obj:`Model`): model
            id (:obj:`str`): id
            name (:obj:`str`): name
            species (:obj:`list of :obj:`Species`): species
            reactions (:obj:`list of :obj:`Reaction`): reactions
            algorithm (:obj:`str`): simulation algorithm
        """
        self.model = model

        self.id = id
        self.name = name

        self.index = None

        self.species = species or []
        self.reactions = reactions or []
        self.parameters = []
        self.algorithm = algorithm

        self._species_counts = numpy.zeros(0)
        self._vol = numpy.zeros(0)
        self._extracellular_vol = numpy.zeros(0)

    def setup(self):
        """ Initialize species counts dictionary """

        self._species_counts = {}
        for species in self.species:
            self._species_counts[species.id] = 0

    def update_local_cell_state(self):
        """ Set local state (e.g., species counts) from global state """
        model = self.model
        for species in self.species:
            self._species_counts[species.id] = model._species_counts[species.species.index, species.compartment.index]
        self._vol = model._vol
        self._extracellular_vol = model._extracellular_vol

    def get_species_concs(self):
        """ Get species concentrations

        Returns:
            :obj:`dict`: dictionary that maps species-compartment ids to concentrations
        """

        vols = self.get_species_vols()
        concs = {}
        for species in self.species:
            concs[species.id] = self._species_counts[species.id] / vols[species.id] / Avogadro
        return concs

    def get_species_vols(self):
        """ Get the volume of the compartment of each species

        Returns:
            :obj:`dict`: dictionary that maps species-compartment ids to the volume of their compartment
        """

        vols = {}
        for species in self.species:
            if species.compartment.id == 'c':
                vols[species.id] = self._vol
            else:
                vols[species.id] = self._extracellular_vol
        return vols

    @staticmethod
    def calc_rxn_rates(rxns, species_concs, species_counts=None):
        """ Calculate reaction rates

        Args:
            rxns (:obj:`list` of :obj:`Reaction`): reactions
            species_concs (:obj:`dict`): dictionary that maps species-compartment ids to concentrations
            species_counts (:obj:`dict`, optional): dictionary that maps species-compartment ids to counts

        Returns:
            :obj:`numpy.ndarray`: reaction rates
        """
        rates = numpy.full(len(rxns), numpy.nan)
        for i_rxn, rxn in enumerate(rxns):
            if rxn.rate_law:
                rates[i_rxn] = max(0, eval(rxn.rate_law.compiled_expr, {}, {
                    'species_concs': species_concs, 'Vmax': rxn.v_max, 'Km_1': rxn.k_m_1, 'Km_2': rxn.k_m_2}))

            if species_counts is not None:
                for part in rxn.participants:
                    if part.coefficient < 0 and species_counts[part.id] < -part.coefficient:
                        rates[i_rxn] = 0
                        break

        return rates

    def get_component_by_id(self, id, components):
        """ Get the submodel component with id :obj:`id`

        Args:
            id (:obj:`str`): component id
            components (:obj:`list` of :obj:`object`): list of components to search over

        Returns:
            :obj:`object`: component
        """
        for component in components:
            if component.id == id:
                return component


class FbaSubmodel(Submodel):
    """ A flux balance analysis (FBA) submodel

    Attributes:
        metabolism_production_rxn (:obj:`dict`): metabolism production reaction
        ex_species (:obj:`list` of :obj:`ExchangedSpecies`): exchanged species

        cobra_model (:obj:`CobraModel`): COBRApy model
        thermodynamic_bounds (:obj:`dict`): upper and lower reversibility bounds
        ex_rate_bounds (:obj:`dict`): upper and lower exchange bounds

        def_fba_bound (:obj:`float`): default FBA bound
        growth_rate_scale (:obj:`float`): scaling factor for coefficients of the metabolism production reaction
        solver (:obj:`str`): optimization solver

        _dry_wt (:obj:`float`): current cellular dry weight
        _rxn_fluxes (:obj:`numpy.ndarray`): current reaction fluxes
        _growth (:obj:`float`): current growth rate
    """

    def __init__(self, model, id=None, name=None, species=None, reactions=None, def_fba_bound=1e15, growth_rate_scale=1e-6, solver='glpk'):
        """
        Args:
            model (:obj:`Model`): model
            id (:obj:`str`): id
            name (:obj:`str`): name
            species (:obj:`list of :obj:`Species`): species
            reactions (:obj:`list of :obj:`Reaction`): reactions
            def_fba_bound (:obj:`float`, optional): default FBA bound
            growth_rate_scale (:obj:`float`, optional): scaling factor for coefficients of the metabolism production reaction
            solver (:obj:`str`, optional): optimization solver
        """
        super(FbaSubmodel, self).__init__(model, id=id, name=name, species=species, reactions=reactions, algorithm='FBA')

        self.metabolism_production_rxn = None
        self.ex_species = None

        self.cobra_model = None
        self.thermodynamic_bounds = None
        self.ex_rate_bounds = None

        self.def_fba_bound = def_fba_bound
        self.growth_rate_scale = growth_rate_scale
        self.solver = solver

        self._dry_wt = numpy.nan
        self._rxn_fluxes = numpy.zeros(0)
        self._growth = numpy.nan

    def setup(self):
        """ Setup reaction participant, enzyme counts matrices """
        super(FbaSubmodel, self).setup()

        '''Setup FBA'''
        cobra_model = CobraModel(self.id)
        self.cobra_model = cobra_model

        # setup metabolites
        cb_mets = []
        for species in self.species:
            cb_mets.append(CobraMetabolite(id=species.id, name=species.name))
        cobra_model.add_metabolites(cb_mets)

        # setup reactions
        for rxn in self.reactions:
            cb_rxn = CobraReaction(
                id=rxn.id,
                name=rxn.name,
                lower_bound=-self.def_fba_bound if rxn.reversible else 0,
                upper_bound=self.def_fba_bound,
            )
            cobra_model.add_reactions([cb_rxn])

            cb_mets = {}
            for part in rxn.participants:
                cb_mets[part.id] = part.coefficient
                if rxn.id == 'MetabolismProduction' and part.species.id != 'Biomass':
                    cb_mets[part.id] *= self.growth_rate_scale
            cb_rxn.add_metabolites(cb_mets)

        # add external exchange reactions
        self.ex_species = []
        for species in self.species:
            if species.compartment.id == 'e':
                cb_rxn = CobraReaction(
                    id='{}Ex'.format(species.species.id),
                    name='{} exchange'.format(species.species.name),
                    lower_bound=-self.def_fba_bound,
                    upper_bound=self.def_fba_bound,
                )
                cobra_model.add_reactions([cb_rxn])
                cb_rxn.add_metabolites({species.id: 1})

                self.ex_species.append(ExchangedSpecies(id=species.id, reaction_index=cobra_model.reactions.index(cb_rxn)))

        # add biomass exchange reaction
        cb_rxn = CobraReaction(
            id='BiomassEx',
            name='Biomass exchange',
            lower_bound=0,
            upper_bound=self.def_fba_bound,
        )
        cobra_model.add_reactions([cb_rxn])
        cb_rxn.add_metabolites({'Biomass[c]': -1})

        '''Bounds'''
        # thermodynamic
        lower_bounds = []
        upper_bounds = []
        for rxn in cobra_model.reactions:
            lower_bounds.append(rxn.lower_bound)
            upper_bounds.append(rxn.upper_bound)
        self.thermodynamic_bounds = {
            'lower': numpy.array(lower_bounds),
            'upper': numpy.array(upper_bounds),
        }

        # exchange reactions
        carbon_ex_rate = self.get_component_by_id('carbonExchangeRate', self.parameters).value
        non_carbon_ex_rate = self.get_component_by_id('nonCarbonExchangeRate', self.parameters).value
        self.ex_rate_bounds = {
            'lower': numpy.full(len(cobra_model.reactions), -numpy.nan),
            'upper': numpy.full(len(cobra_model.reactions), numpy.nan),
        }
        for ex_species in self.ex_species:
            if self.get_component_by_id(ex_species.id, self.species).species.has_carbon():
                self.ex_rate_bounds['lower'][ex_species.reaction_index] = -carbon_ex_rate
                self.ex_rate_bounds['upper'][ex_species.reaction_index] = carbon_ex_rate
            else:
                self.ex_rate_bounds['lower'][ex_species.reaction_index] = -non_carbon_ex_rate
                self.ex_rate_bounds['upper'][ex_species.reaction_index] = non_carbon_ex_rate

        '''Setup reactions'''
        self.metabolism_production_rxn = {
            'index': cobra_model.reactions.index(cobra_model.reactions.get_by_id('MetabolismProduction')),
            'reaction': self.get_component_by_id('MetabolismProduction', self.reactions),
        }

        cobra_model.objective = 'MetabolismProduction'
        cobra_model.solver = self.solver

    def update_local_cell_state(self):
        """ Set local state (e.g., species counts) from global state """
        super(FbaSubmodel, self).update_local_cell_state()
        self._dry_wt = self.model._dry_wt

    def calc_rxn_bounds(self, time_step=1):
        """ Calculate reaction bounds

        Args:
            time_step (:obj:`float`): time step (s)
        """

        # thermodynamics
        lo_bounds = self.thermodynamic_bounds['lower'].copy()
        hi_bounds = self.thermodynamic_bounds['upper'].copy()

        # rate laws
        hi_bounds[0:len(self.reactions)] = util.nan_minimum(
            hi_bounds[0:len(self.reactions)],
            self.calc_rxn_rates(self.reactions, self.get_species_concs()) * self._vol * Avogadro,
        )

        # external nutrients availability
        for ex_species in self.ex_species:
            hi_bounds[ex_species.reaction_index] = max(0, numpy.minimum(
                hi_bounds[ex_species.reaction_index], self._species_counts[ex_species.id]) / time_step)

        # exchange bounds
        lo_bounds = util.nan_maximum(lo_bounds, self._dry_wt / 3600 * Avogadro * 1e-3 * self.ex_rate_bounds['lower'])
        hi_bounds = util.nan_minimum(hi_bounds, self._dry_wt / 3600 * Avogadro * 1e-3 * self.ex_rate_bounds['upper'])

        # return
        for i_rxn, rxn in enumerate(self.cobra_model.reactions):
            rxn.lower_bound = lo_bounds[i_rxn]
            rxn.upper_bound = hi_bounds[i_rxn]


class SsaSubmodel(Submodel):
    """ A stochastic simulation algorithm (SSA) submodel """

    def __init__(self, model, id=None, name=None, species=None, reactions=None):
        """
        Args:
            model (:obj:`Model`): model
            id (:obj:`str`): id
            name (:obj:`str`): name
            species (:obj:`list of :obj:`Species`): species
            reactions (:obj:`list of :obj:`Reaction`): reactions
        """
        super(SsaSubmodel, self).__init__(model, id=id, name=name, species=species, reactions=reactions, algorithm='SSA')


class Compartment(object):
    """ A compartment

    Attributes:
        id (:obj:`str`): id
        name (:obj:`str`): name
        index (:obj:`int`): index of the reaction within the list of reactions
        init_vol (:obj:`float`): initial volume
        comments (:obj:`str`): comments
    """

    def __init__(self, id=None, name=None, init_vol=None, comments=None):
        """
        Args:
            id (:obj:`str`): id
            name (:obj:`str`): name
            init_vol (:obj:`float`): initial volume
            comments (:obj:`str`): comments
        """
        self.id = id
        self.name = name

        self.index = None

        self.init_vol = init_vol
        self.comments = comments


class SpeciesType(str, enum.Enum):
    """ Type of species """
    metabolite = 'Metabolite'
    rna = 'RNA'
    protein = 'Protein'
    complex = 'Complex'
    pseudo_species = 'Pseudo species'


class Species(object):
    """ A species

    Attributes:
        id (:obj:`str`): id
        name (:obj:`str`): name
        index (:obj:`int`): index of the reaction within the list of reactions
        structure (:obj:`str`): SMILES or InChI structure of IUPAC or IUBMB sequence
        chemical_formula (:obj:`str`): chemical formula
        mol_wt (:obj:`float`): molecular weight
        charge (:obj:`int`): charge
        type (:obj:`SpeciesType`): type
        concentrations (:obj:`list` of :obj:`Concentration`): concentrations
        identifiers (:obj:`list` of :obj:`Indentifier`): identifiers
        comments (:obj:`str`): comments
    """

    def __init__(self, id=None, name=None, structure=None, chemical_formula=None, mol_wt=None,
                 charge=None, type=None, concentrations=None, identifiers=None, comments=None):
        """
        Args:
            id (:obj:`str`): id
            name (:obj:`str`): name
            structure (:obj:`str`): SMILES or InChI structure of IUPAC or IUBMB sequence
            chemical_formula (:obj:`str`): chemical formula
            mol_wt (:obj:`float`): molecular weight
            charge (:obj:`int`): charge
            type (:obj:`SpeciesType`): type
            concentrations (:obj:`list` of :obj:`Concentration`): concentrations
            identifiers (:obj:`list` of :obj:`Indentifier`): identifiers
            comments (:obj:`str`): comments
        """

        self.id = id
        self.name = name

        self.index = None

        self.structure = structure
        self.chemical_formula = chemical_formula
        self.mol_wt = mol_wt
        self.charge = charge
        self.type = type
        self.concentrations = concentrations or []
        self.identifiers = identifiers or []
        self.comments = comments

    def has_carbon(self):
        if self.chemical_formula:
            return self.chemical_formula.upper().find('C') != -1
        return False


class Reaction(object):
    """ A reaction

    Attributes:
        id (:obj:`str`): id
        name (:obj:`str`): name
        index (:obj:`int`): index of the reaction within the list of reactions
        index_in_submodel (:obj:`int`): index of the reaction within the list of reactions of its submodel
        submodel (:obj:`Submodel`): submodel
        participants (:obj:`list` of :obj:`ReactionParticipant`): participants
        reversible (:obj:`bool`): reverisibility
        enzyme (:obj:`SpeciesCompartment`): enzyme
        rate_law (:obj:`RateLaw`): rate law
        v_max (:obj:`float`): Vmax
        k_m_1 (:obj:`float`): Km
        k_m_2 (:obj:`float`): second Km
        identifiers (:obj:`list` of :obj:`Indentifier`): identifiers
        comments (:obj:`str`): comments
    """

    def __init__(self, id=None, name=None, submodel=None, participants=None, reversible=None,
                 enzyme=None, rate_law=None, v_max=None, k_m_1=None, k_m_2=None, identifiers=None, comments=None):
        """
        Args:
            id (:obj:`str`): id
            name (:obj:`str`): name
            submodel (:obj:`Submodel`): submodel
            participants (:obj:`list` of :obj:`ReactionParticipant`): participants
            reversible (:obj:`bool`): reverisibility
            enzyme (:obj:`SpeciesCompartment`): enzyme
            rate_law (:obj:`RateLaw`): rate law
            v_max (:obj:`float`): Vmax
            k_m_1 (:obj:`float`): Km
            k_m_2 (:obj:`float`): second Km
            identifiers (:obj:`list` of :obj:`Indentifier`): identifiers
            comments (:obj:`str`): comments
        """

        if v_max:
            v_max = float(v_max)
        if k_m_1:
            k_m_1 = float(k_m_1)
        if k_m_2:
            k_m_2 = float(k_m_2)

        self.id = id
        self.name = name

        self.index = None
        self.index_in_submodel = None

        self.submodel = submodel
        self.participants = participants or []
        self.reversible = reversible
        self.enzyme = enzyme
        self.rate_law = rate_law
        self.v_max = v_max
        self.k_m_1 = k_m_1
        self.k_m_2 = k_m_2
        self.identifiers = identifiers or []
        self.comments = comments


class Parameter(object):
    """ A model parameter

    Attributes:
        id (:obj:`str`): id
        name (:obj:`str`): name
        index (:obj:`int`): index of the parameter within the list of parameters
        submodel (:obj:`Submodel`): submodel
        value (:obj:`float`): value
        units (:obj:`str`): units
        comments (:obj:`str`): comments
    """

    def __init__(self, id=None, name=None, submodel=None, value=None, units=None, comments=None):
        """
        Args:
            id (:obj:`str`): id
            name (:obj:`str`): name
            submodel (:obj:`Submodel`, optional): submodel
            value (:obj:`float`): value
            units (:obj:`str`): units
            comments (:obj:`str`, optional): comments
        """
        self.id = id
        self.name = name

        self.index = None

        self.submodel = submodel
        self.value = value
        self.units = units
        self.comments = comments


class Reference(object):
    """ A reference

    Attributes
        id (:obj:`str`): id
        name (:obj:`str`): name
        index (:obj:`int`): index of the reference within the list of references
        indentifiers (:obj:`list` of :obj:`Identifier`): identifiers
        comments (:obj:`str`): comments
    """

    def __init__(self, id=None, name=None, identifiers=None, comments=None):
        """
        Args:
            id (:obj:`str`): id
            name (:obj:`str`): name
            indentifiers (:obj:`list` of :obj:`Identifier`, optional): identifiers
            comments (:obj:`str`, optional): comments
        """
        self.id = id
        self.name = name

        self.index = None

        self.identifiers = identifiers or []
        self.comments = comments


class Concentration(object):
    """ A concentration of a species in a compartment

    Attributes:
        compartment (:obj:`Compartment`): compartment
        value (:obj:`float`): concentration
    """

    def __init__(self, compartment=None, value=None):
        """
        Args:
            compartment (:obj:`Compartment`): compartment
            value (:obj:`float`): concentration
        """
        self.compartment = compartment
        self.value = value


class SpeciesCompartment(object):
    """ A tuple of a species and a compartment

    Attributes:
        id (:obj:`str`): id
        name (:obj:`str`): name
        index (:obj:`int`): index of the species-compartment in the array of species-compartments
        species (:obj:`Species`): species
        compartment (:obj:`Compartment`): compartment
    """

    def __init__(self, index=None, species=None, compartment=None):
        """
        Args:
            index (:obj:`int`): index of the species-compartment in the array of species-compartments
            species (:obj:`Species`): species
            compartment (:obj:`Compartment`): compartment
        """
        self.id = None
        self.name = None

        self.index = index

        self.species = species
        self.compartment = compartment

    def get_id_name(self):
        """ Get the id and name of the species-compartment

        Returns:
            :obj:`tuple`:

                * :obj:`str`: id
                * :obj:`str`: name
        """
        return (
            '{}[{}]'.format(self.species.id, self.compartment.id),
            '{} ({})'.format(self.species.name, self.compartment.name),
        )


class ExchangedSpecies(object):
    """ An external species

    Attributes:
        id (:obj:`str`): id
        reaction_index (:obj:`int`): index of the exchange reaction for the species
    """

    def __init__(self, id=None, reaction_index=None):
        """
        Args:
            id (:obj:`str`): id
            reaction_index (:obj:`int`): index of the exchange reaction for the species
        """
        self.id = id
        self.reaction_index = reaction_index


class ReactionParticipant(object):
    """ A participant in a reaction

    Attributes
        id (:obj:`str`): id
        name (:obj:`str`): name
        species (:obj:`Species`): species
        compartment (:obj:`Compartment`): compartment
        coefficient (:obj:`float`): coefficient
        reaction (:obj:`Reaction`): reaction that the participant participates in
    """

    def __init__(self, species=None, compartment=None, coefficient=None):
        """
        Args:
            species (:obj:`Species`): species
            compartment (:obj:`Compartment`): compartment
            coefficient (:obj:`float`): coefficient
        """
        self.id = None
        self.name = None

        self.species = species
        self.compartment = compartment
        self.coefficient = coefficient

        self.reaction = None

    def get_id_name(self):
        """ Get the id and name of the reaction participant

        Returns:
            :obj:`tuple`:

                * :obj:`str`: id
                * :obj:`str`: name
        """
        return (
            '{}[{}]'.format(self.species.id, self.compartment.id),
            '{} ({})'.format(self.species.name, self.compartment.name),
        )


class RateLaw(object):
    """ A rate law of a reaction

    Attributes:
        expr (:obj:`str`): native rate law
        compiled_expr (:obj:`str`): Python expression for the rate law
    """

    def __init__(self, expr=None):
        """
        Args:
            expr (:obj:`str`): native rate law
        """
        self.expr = expr
        self.compiled_expr = None

    def get_modifiers(self, species, compartments):
        """ Get the ids of the modifiers of the rate law

        Args:
            species (:obj:`list` of :obj:`Species`): species
            compartments (:obj:`list` of :obj:`Compartment`): compartments

        Returns:
            :obj:`list` of :obj:`str`: list of the identifiers of the modifiers of a rate law (e.g., reactants and enzymes)
        """
        modifiers = []
        for spec in species:
            for comp in compartments:
                id = '{}[{}]'.format(spec.id, comp.id)
                if self.expr.find(id) != -1:
                    modifiers.append(id)
        return modifiers

    def compile(self, species, compartments):
        """ Compile the native rate law into a Python expression

        Args:
            species (:obj:`list` of :obj:`Species`): species
            compartments (:obj:`list` of :obj:`Compartment`): compartments

        Returns:
            :obj:`str`: compiled Python expression
        """

        compiled_expr = self.expr

        for spec in species:
            for comp in compartments:
                id = '{}[{}]'.format(spec.id, comp.id)
                compiled_expr = compiled_expr.replace(id, "species_concs['{}']".format(id))

        return compiled_expr


class Identifier(object):
    """ An identifier of an entry in an external database

    Attributes:
        namespace (:obj:`str`): namespace
        id (:obj:`str`): id in namespace
    """

    def __init__(self, namespace=None, id=None):
        """
        Args:
            namespace (:obj:`str`): namespace
            id (:obj:`str`): id in namespace
        """
        self.namespace = namespace
        self.id = id


def read_model_from_file(file_name):
    """ Read model from an XLSX spreadsheet into a Python object

    Args:
        file_name (:obj:`str`): path to a XLSX spreadsheet file that defines a model

    Returns:
        :obj:`Model`: model
    """
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", "Discarded range with reserved name", UserWarning)
        wb = load_workbook(filename=file_name)

    # initialize model object
    model = Model()

    '''Read details from spreadsheet'''
    # submodels
    ws = wb['Submodels']
    for i_row in range(2, ws.max_row + 1):
        id = ws.cell(row=i_row, column=1).value
        name = ws.cell(row=i_row, column=2).value
        framework = ws.cell(row=i_row, column=3).value
        if framework == 'constraint-based':
            submodel = FbaSubmodel(model, id=id, name=name)
        elif framework == 'discrete kinetic':
            submodel = SsaSubmodel(model, id=id, name=name)
        else:
            raise ValueError('Undefined framework "{}" for submodel "{}"'.format(framework, id))
        model.submodels.append(submodel)

    # compartments
    ws = wb['Compartments']
    for i_row in range(2, ws.max_row + 1):
        model.compartments.append(Compartment(
            id=ws.cell(row=i_row, column=1).value,
            name=ws.cell(row=i_row, column=2).value,
            init_vol=float(ws.cell(row=i_row, column=3).value),
            comments=ws.cell(row=i_row, column=4).value,
        ))

    # species
    ws = wb['Species']
    for i_row in range(2, ws.max_row + 1):
        mw_str = ws.cell(row=i_row, column=5).value
        if mw_str:
            mw = float(mw_str)
        else:
            mw = None

        charge_str = ws.cell(row=i_row, column=6).value
        if charge_str:
            charge = float(charge_str)
        else:
            charge = None

        model.species.append(Species(
            id=ws.cell(row=i_row, column=1).value,
            name=ws.cell(row=i_row, column=2).value,
            structure=ws.cell(row=i_row, column=3).value,
            chemical_formula=ws.cell(row=i_row, column=4).value,
            mol_wt=mw,
            charge=charge,
            type=SpeciesType(ws.cell(row=i_row, column=7).value),
            concentrations=[
                Concentration(compartment='c', value=float(ws.cell(row=i_row, column=8).value or 0)),
                Concentration(compartment='e', value=float(ws.cell(row=i_row, column=9).value or 0)),
            ],
            identifiers=[
                Identifier(
                    namespace=ws.cell(row=i_row, column=10).value,
                    id=ws.cell(row=i_row, column=11).value,
                ),
            ],
            comments=ws.cell(row=i_row, column=12).value,
        ))

    # reactions
    ws = wb['Reactions']

    for i_row in range(2, ws.max_row + 1):
        participants = parse_rxn_eq(ws.cell(row=i_row, column=4).value)

        rate_law_str = ws.cell(row=i_row, column=7).value
        if rate_law_str:
            rate_law = RateLaw(rate_law_str)
        else:
            rate_law = None

        rxn = Reaction(
            id=ws.cell(row=i_row, column=1).value,
            name=ws.cell(row=i_row, column=2).value,
            submodel=ws.cell(row=i_row, column=3).value,
            participants=participants,
            reversible=ws.cell(row=i_row, column=5).value,
            enzyme=ws.cell(row=i_row, column=6).value,
            rate_law=rate_law,
            v_max=ws.cell(row=i_row, column=8).value,
            k_m_1=ws.cell(row=i_row, column=10).value,
            k_m_2=ws.cell(row=i_row, column=12).value,
            identifiers=[
                Identifier(
                    namespace=ws.cell(row=i_row, column=13).value,
                    id=ws.cell(row=i_row, column=14).value,
                ),
            ],
            comments=ws.cell(row=i_row, column=15).value,
        )
        for part in participants:
            part.reaction = rxn
        model.reactions.append(rxn)

    # parameters
    ws = wb['Parameters']
    for i_row in range(2, ws.max_row + 1):
        model.parameters.append(Parameter(
            id=ws.cell(row=i_row, column=1).value,
            name=ws.cell(row=i_row, column=2).value,
            submodel=ws.cell(row=i_row, column=3).value,
            value=float(ws.cell(row=i_row, column=4).value),
            units=ws.cell(row=i_row, column=5).value,
            comments=ws.cell(row=i_row, column=6).value,
        ))

    # references
    ws = wb['References']
    for i_row in range(2, ws.max_row + 1):
        model.references.append(Reference(
            id=ws.cell(row=i_row, column=1).value,
            name=ws.cell(row=i_row, column=2).value,
            identifiers=[
                Identifier(
                    namespace=ws.cell(row=i_row, column=3).value,
                    id=ws.cell(row=i_row, column=4).value,
                ),
            ],
            comments=ws.cell(row=i_row, column=5).value,
        ))

    '''set component indices'''
    model.set_component_indices()

    '''deserialize references'''
    undefined_components = []

    # species concentration
    for species in model.species:
        for conc in species.concentrations:
            id = conc.compartment
            obj = model.get_component_by_id(id, model.compartments)
            if id and obj is None:
                undefined_components.append(id)
            conc.compartment = obj

    # reaction submodel, participant species, participant compartments, enzymes
    for reaction in model.reactions:
        id = reaction.submodel
        obj = model.get_component_by_id(id, model.submodels)
        if id and obj is None:
            undefined_components.append(id)
        reaction.submodel = obj

        for part in reaction.participants:
            id = part.species
            obj = model.get_component_by_id(id, model.species)
            if id and obj is None:
                undefined_components.append(id)
            part.species = obj

            id = part.compartment
            obj = model.get_component_by_id(id, model.compartments)
            if id and obj is None:
                undefined_components.append(id)
            part.compartment = obj

            part.id, part.name = part.get_id_name()

        if reaction.enzyme:
            match = re.match(r'^(.*?)\[(.*?)\]$', reaction.enzyme)
            species = model.get_component_by_id(match.group(1), model.species)
            comp = model.get_component_by_id(match.group(2), model.compartments)
            if species is None:
                undefined_components.append(match.group(1))
            if comp is None:
                undefined_components.append(match.group(2))
            reaction.enzyme = SpeciesCompartment(species=species, compartment=comp)
            reaction.enzyme.id, reaction.enzyme.name = reaction.enzyme.get_id_name()

    # parameter submodels
    for param in model.parameters:
        id = param.submodel
        if id:
            obj = model.get_component_by_id(id, model.submodels)
            if obj is None:
                undefined_components.append(id)
            param.submodel = obj

    if len(undefined_components) > 0:
        undefined_components = list(set(undefined_components))
        undefined_components.sort()
        raise ValueError('Undefined components:\n- {}'.format('\n- '.join(undefined_components)))

    ''' Assemble back references'''
    for submodel in model.submodels:
        submodel.reactions = []
        submodel.species = []
        submodel.parameters = []
    for rxn in model.reactions:
        rxn.submodel.reactions.append(rxn)
        rxn.index_in_submodel = len(rxn.submodel.reactions) - 1
        for part in rxn.participants:
            rxn.submodel.species.append('{}[{}]'.format(part.species.id, part.compartment.id))
        if rxn.enzyme:
            rxn.submodel.species.append(rxn.enzyme.id)
        if rxn.rate_law:
            rxn.submodel.species += rxn.rate_law.get_modifiers(model.species, model.compartments)

    for param in model.parameters:
        if param.submodel:
            param.submodel.parameters.append(param)

    for submodel in model.submodels:
        species_str_arr = list(set(submodel.species))
        species_str_arr.sort()
        submodel.species = []
        for index, species_str in enumerate(species_str_arr):
            species_id, comp_id = species_str.split('[')
            comp_id = comp_id[0:-1]
            species_comp = SpeciesCompartment(
                index=index,
                species=model.get_component_by_id(species_id, model.species),
                compartment=model.get_component_by_id(comp_id, model.compartments),
            )
            species_comp.id, species_comp.name = species_comp.get_id_name()
            submodel.species.append(species_comp)

    '''Transcode rate laws'''
    for rxn in model.reactions:
        if rxn.rate_law:
            rxn.rate_law.compiled_expr = rxn.rate_law.compile(model.species, model.compartments)

    '''Prepare submodels for computation'''
    model.setup()

    '''Return'''
    return model


def parse_rxn_eq(eq_str):
    """ Parse a string representation of a reaction equation into a Python data structure

    Args:
        eq_str (:obj:`str`): string representation of a reaction

    Returns:
        :obj:`list` of :obj:`ReactionParticipant`: list of reaction participants and their stoichiometries
    """

    # Split stoichiometry in to global compartment, left-hand side, right-hand side, reversibility indictor
    match = re.match((r'(?P<compartment>\[([a-z])\]: )?'
                      r'(?P<lhs>((\(\d*\.?\d*([e][-+]?[0-9]+)?\) )?[a-z0-9\-_]+(\[[a-z]\])? \+ )*'
                      r'(\(\d*\.?\d*([e][-+]?[0-9]+)?\) )?[a-z0-9\-_]+(\[[a-z]\])?) '
                      r'<=> '
                      r'(?P<rhs>((\(\d*\.?\d*([e][-+]?[0-9]+)?\) )?[a-z0-9\-_]+(\[[a-z]\])? \+ )*'
                      r'(\(\d*\.?\d*([e][-+]?[0-9]+)?\) )?[a-z0-9\-_]+(\[[a-z]\])?)'),
                     eq_str, flags=re.I)
    if match is None:
        raise ValueError('Invalid reaction equation: {}'.format(eq_str))

    # Determine reversiblity
    eq_dict = match.groupdict()

    # Determine if global compartment for reaction was specified
    if eq_dict['compartment'] is None:
        global_comp = None
    else:
        global_comp = re.match(r'\[(?P<compartment>[a-z])\]', eq_dict['compartment'], flags=re.I).groupdict()['compartment']

    # initialize array of reaction participants
    participants = []

    # Parse left-hand side
    for part_str in eq_dict['lhs'].split(' + '):
        part_dict = re.match(
            r'(\((?P<coefficient>\d*\.?\d*([e][-+]?[0-9]+)?)\) )?(?P<species>[a-z0-9\-_]+)(\[(?P<compartment>[a-z])\])?',
            part_str, flags=re.I).groupdict()

        species = part_dict['species']
        compartment = part_dict['compartment'] or global_comp
        coefficient = float(part_dict['coefficient'] or 1)

        if coefficient == 0:
            continue

        participants.append(ReactionParticipant(
            species=species,
            compartment=compartment,
            coefficient=-coefficient,
        ))

    # Parse right-hand side
    for part_str in eq_dict['rhs'].split(' + '):
        part_dict = re.match(
            r'(\((?P<coefficient>\d*\.?\d*([e][-+]?[0-9]+)?)\) )?(?P<species>[a-z0-9\-_]+)(\[(?P<compartment>[a-z])\])?',
            part_str, flags=re.I).groupdict()

        species = part_dict['species']
        compartment = part_dict['compartment'] or global_comp
        coefficient = float(part_dict['coefficient'] or 1)

        if coefficient == 0:
            continue

        participants.append(ReactionParticipant(
            species=species,
            compartment=compartment,
            coefficient=coefficient,
        ))

    # return participants
    return participants
