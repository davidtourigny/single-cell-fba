# Copyright (C) 2019, 2020 Columbia University Irving Medical Center,
#     New York, USA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

import cobra
import os.path
import ssafba
import ssafba.jit


def simple_model():
    # init model with name = "test", SSA-only reactions = 2, SSA-FBA reaction = 1 , dependent FBA bounds = 1
    model = ssafba.SsaFba("test", 2, 1, 1)

    # read in cobra model
    cobra_model = cobra.io.read_sbml_model(os.path.join(os.path.dirname(__file__), "Metabolism.xml"))

    # set initial counts for substrates S_0, S_1, S_2
    substrates = [1000, 0, 0]

    # set stoichiometry for reactions v_0, v_1, v_2
    model.set_stoichiometry(0, [0, 1], [-1, 2])  # metabolite indices and stoichiometry values for v_0: S_0 -> 2 * S_1
    model.set_stoichiometry(1, [0, 1], [1, -2])  # metabolite indices and stoichiometry values for v_1: S_0 <- 2 * S_1
    model.set_stoichiometry(2, [1, 2], [-2, 1])  # metabolite indices and stoichiometry values for v_2: 2 * S_1 -> S_2

    # set reactant indices for reactions v_0, v_1, v_2
    model.set_reactants(0, [0])  # reactant for v_0 is S_0
    model.set_reactants(1, [1])  # reactant for v_1 is S_1
    model.set_reactants(2, [1])  # reactant for v_2 is S_1

    # set dependent reactions for reactions v_0, v_1, v_2
    model.set_dependencies(0, [0, 1, 2])  # dependents for v_0 are v_0, v_1, v_2
    model.set_dependencies(1, [0, 1, 2])  # dependents for v_1 are v_0, v_1, v_2
    model.set_dependencies(2, [1, 2])  # dependents for v_2 are v_1, v_2

    # set GLPK column index for Growth rate as propensity of SSA-FBA reaction v_2
    model.set_ssafba_indices([2545])

    # compile shared library for function definitions
    write_library(model.name)
    ssafba.jit.compile()

    # simulate the model with max events = 1e5, duration = 1.0e6, user_data = 2131 (O2ex_reverse GLPK column index)
    result = ssafba.simulate_model(cobra_model, model, substrates, 3000, 1.0e6, delta=500, fast=False, user_data=2131)


def test(omega=0.9, nevnts=10000, max_npevnt=5):
    cobra_model = cobra.io.read_sbml_model(os.path.join(os.path.dirname(__file__), "Metabolism.xml"))
    ssafba.test_speed(cobra_model, omega, nevnts, max_npevnt)
    ssafba.toy_simulation(cobra_model, 2545, 2131)  # (Growth idx = 2545, O2ex_reverse idx = 2131)


def write_indent(the_file, indent_count, text):
    """Write indent

        Parameters
        -------
        the_file : file
            Open cpp file.
        indent_count : int
            Number of tabs to indent.
        text : string
            The line of text to write to file.
    """
    while indent_count > 0:
        text = "    " + text
        indent_count -= 1
    the_file.write(text)


def write_library(model_name):
    cpp_file_path = os.path.join("functionlib.cpp")
    with open(cpp_file_path, "w") as cpp_file:
        cpp_file.write(
            "#include <pybind11/pybind11.h> \n#include <vector> \nnamespace py = pybind11; \n\n"
        )
    cpp_file_path = os.path.join("functionlib.cpp")
    with open(cpp_file_path, "a") as cpp_file:
        write_indent(
            cpp_file,
            0,
            'extern "C" double ssa_propensities_'
            + model_name
            + "(int reaction, std::vector<int64_t> reactants, void* user_data)\n",
        )
        write_indent(cpp_file, 0, "{\n")
        write_indent(cpp_file, 1, "double value = 0.0;\n")
        write_indent(cpp_file, 1, "if(reaction == 0){\n")
        write_indent(cpp_file, 2, "value = reactants[0]*0.0002;\n")
        write_indent(cpp_file, 1, "}\n")
        write_indent(cpp_file, 1, "else if(reaction == 1){\n")
        write_indent(cpp_file, 2, "value = reactants[0]*reactants[0]*0.0002;\n")
        write_indent(cpp_file, 1, "}\n")
        write_indent(cpp_file, 1, "return value;\n")
        write_indent(cpp_file, 0, "}\n\n")
        write_indent(
            cpp_file,
            0,
            'extern "C" void fba_bounds_'
            + model_name
            + "(std::vector<int> reaction_array, std::vector< std::vector<int64_t> > reactants_array, void* user_data, std::vector<int> &index, std::vector<double> &value)\n",
        )
        write_indent(cpp_file, 0, "{\n")
        write_indent(cpp_file, 1, "int reaction = reaction_array[0];\n")
        write_indent(cpp_file, 1, "std::vector<int64_t> reactants = reactants_array[0];\n")
        write_indent(cpp_file, 1, "if(reaction == 2){\n")
        write_indent(cpp_file, 2, "py::object user_obj = *((py::object*) user_data);\n")
        write_indent(cpp_file, 2, "int bound_idx = user_obj.cast<int>();\n")
        write_indent(cpp_file, 2, "index = std::vector<int>(1,bound_idx);\n")
        write_indent(cpp_file, 2, "value = std::vector<double>(1,0.1*reactants[0]);\n")
        write_indent(cpp_file, 1, "}\n")
        write_indent(cpp_file, 0, "}\n\n")

if __name__=="__main__":
    test()
